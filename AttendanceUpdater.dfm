object FAttUpd: TFAttUpd
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsNone
  ClientHeight = 257
  ClientWidth = 611
  Color = 4209709
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 16
    Width = 581
    Height = 224
    BevelWidth = 4
    TabOrder = 0
    object Label7: TLabel
      Left = 16
      Top = 10
      Width = 258
      Height = 34
      Caption = 'Update attendances'
      Font.Charset = ANSI_CHARSET
      Font.Color = 2861823
      Font.Height = -27
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 42
      Width = 551
      Height = 18
      Caption = 
        'The program will try to automatically update the data from the g' +
        'ame sheets on the internet.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu Medium'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 58
      Width = 436
      Height = 18
      Caption = 
        'Existing data will be backed up. Be sure you'#39're connected to the' +
        ' internet.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu Medium'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 90
      Width = 126
      Height = 18
      Caption = 'This will take a while.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu Medium'
      Font.Style = []
      ParentFont = False
    end
    object stat2: TLabel
      Left = 16
      Top = 138
      Width = 120
      Height = 18
      Caption = 'backing up data'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Ubuntu Mono'
      Font.Style = []
      ParentFont = False
    end
    object stat1: TLabel
      Left = 16
      Top = 122
      Width = 176
      Height = 17
      Caption = 'Initializing update...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Ubuntu Mono'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object closeB: TButton
      Left = 408
      Top = 176
      Width = 153
      Height = 34
      Caption = 'Close'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -21
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = closeBClick
    end
  end
  object downloadT: TTimer
    Enabled = False
    Interval = 200
    OnTimer = downloadTTimer
    Left = 456
    Top = 112
  end
end
