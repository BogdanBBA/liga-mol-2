====================================================================================
      LIGA MOL by BogdyBBA
      Exported Saturday, 19 January 2013, at 0:04
====================================================================================
------------------------------------------------------------------------------------
    STANDINGS
------------------------------------------------------------------------------------
    Team         MP | 3p  2p  1p  0p |     Goals     |  Pts
------------------------------------------------------------------------------------
1. Dunaujvaros   38 | 28   3   2   5 |  165-82   +83 |  92
2. Miskolc       37 | 20   2   4  11 | 158-101   +57 |  68
3. Csikszereda   37 | 19   3   4  11 | 121-106   +15 |  67
4. Nove Zamky    38 | 19   3   1  15 | 126-113   +13 |  64
5. [Corona]      37 | 16   5   2  14 | 121-106   +15 |  60
6. Ferencvaros   37 |  5   4   3  25 |  92-155   -63 |  26
7. Ujpest        38 |  2   2   6  28 |  66-186  -120 |  16
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
    Home matches for CORONA
------------------------------------------------------------------------------------
 ID  |       Date        |      Against | Score |    Periods
------------------------------------------------------------------------------------
2.   |  7 September 2012 |       Ujpest |  4-2  | 0-1, 2-0, 2-1
5.   |  9 September 2012 |   Nove Zamky |  1-2  | 1-0, 0-1, 0-0, 0-1
7.   | 11 September 2012 |  Csikszereda |  3-2  | 1-1, 1-1, 1-0
19.  | 21 September 2012 |  Ferencvaros |  4-1  | 1-0, 2-0, 1-1
22.  | 23 September 2012 |      Miskolc |  3-2  | 2-1, 1-1, 0-0
42.  |   12 October 2012 |  Csikszereda |  2-4  | 0-1, 0-0, 2-3
48.  |   15 October 2012 |  Dunaujvaros |  1-4  | 0-2, 1-1, 0-1
66.  |   2 November 2012 |   Nove Zamky |  2-5  | 1-1, 0-2, 1-2
69.  |   4 November 2012 |      Miskolc |  3-5  | 1-0, 1-2, 1-3
110. |  25 November 2012 |   Nove Zamky |  5-0  | 1-0, 1-0, 3-0
107. |  26 November 2012 |       Ujpest |  5-1  | 2-0, 2-0, 1-1
90.  |  30 November 2012 |  Ferencvaros |  2-1  | 2-1, 0-0, 0-0
93.  |   2 December 2012 |      Miskolc |  6-2  | 3-0, 2-2, 1-0
95.  |   4 December 2012 |  Csikszereda |  5-2  | 3-0, 1-1, 1-1
87.  |  28 December 2012 |  Dunaujvaros |  2-1  | 0-1, 0-0, 1-0, 0-0, 1-0
84.  |  30 December 2012 |       Ujpest |  5-2  | 2-0, 2-1, 1-1
121. |   11 January 2013 |  Csikszereda |  0-2  | 0-1, 0-0, 0-1
124. |   13 January 2013 |  Ferencvaros |  7-1  | 0-1, 4-0, 3-0
131. |   18 January 2013 |   Nove Zamky |  4-0  | 3-0, 0-0, 1-0
134. |   20 January 2013 |  Dunaujvaros |   -   | ---
146. |   1 February 2013 |      Miskolc |   -   | ---
149. |   3 February 2013 |       Ujpest |   -   | ---
164. |  24 February 2013 |  Dunaujvaros |   -   | ---
167. |  25 February 2013 |  Ferencvaros |   -   | ---
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
    RESULTS
------------------------------------------------------------------------------------
 ID  |   Date      |         Away - Home         | Score |    Periods
------------------------------------------------------------------------------------
3.   |  7/Sep/2012 |      Miskolc - Ferencvaros  |  5-2  | 3-1, 2-0, 0-1
1.   |  7/Sep/2012 |  Csikszereda - Nove Zamky   |  2-3  | 1-1, 1-2, 0-0
2.   |  7/Sep/2012 |     [Corona] - Ujpest       |  4-2  | 0-1, 2-0, 2-1
6.   |  9/Sep/2012 |  Dunaujvaros - Miskolc      |  3-0  | 1-0, 1-0, 1-0
4.   |  9/Sep/2012 |  Csikszereda - Ujpest       |  5-0  | 1-0, 2-0, 2-0
5.   |  9/Sep/2012 |     [Corona] - Nove Zamky   |  1-2  | 1-0, 0-1, 0-0, 0-1
8.   | 10/Sep/2012 |  Ferencvaros - Dunaujvaros  |  2-7  | 0-3, 1-4, 1-0
7.   | 11/Sep/2012 |     [Corona] - Csikszereda  |  3-2  | 1-1, 1-1, 1-0
9.   | 14/Sep/2012 |   Nove Zamky - Csikszereda  |  5-3  | 2-2, 2-0, 1-1
91.  | 14/Sep/2012 |       Ujpest - Dunaujvaros  |  3-7  | 1-1, 0-6, 2-0
10.  | 14/Sep/2012 |  Ferencvaros - [Corona]     |  1-6  | 0-2, 0-2, 1-2
13.  | 16/Sep/2012 |  Dunaujvaros - [Corona]     |  7-2  | 3-0, 4-2, 0-0
14.  | 16/Sep/2012 |      Miskolc - Ujpest       |  5-0  | 1-0, 1-0, 3-0
12.  | 16/Sep/2012 |  Ferencvaros - Csikszereda  |  2-4  | 1-1, 0-2, 1-1
17.  | 17/Sep/2012 |   Nove Zamky - Ujpest       |  7-1  | 0-0, 6-1, 1-0
15.  | 17/Sep/2012 |  Dunaujvaros - Csikszereda  |  1-2  | 0-2, 0-0, 1-0
16.  | 17/Sep/2012 |      Miskolc - [Corona]     |  6-0  | 3-0, 2-0, 1-0
20.  | 21/Sep/2012 |   Nove Zamky - Dunaujvaros  |  3-4  | 0-1, 1-1, 2-2
18.  | 21/Sep/2012 |  Csikszereda - Miskolc      |  3-4  | 2-1, 1-1, 0-1, 0-0, 0-1
19.  | 21/Sep/2012 |     [Corona] - Ferencvaros  |  4-1  | 1-0, 2-0, 1-1
21.  | 23/Sep/2012 |  Csikszereda - Ferencvaros  |  4-5  | 2-1, 0-2, 2-2
23.  | 23/Sep/2012 |       Ujpest - Nove Zamky   |  1-4  | 0-1, 1-2, 0-1
22.  | 23/Sep/2012 |     [Corona] - Miskolc      |  3-2  | 2-1, 1-1, 0-0
25.  | 24/Sep/2012 |       Ujpest - Dunaujvaros  |  0-4  | 0-0, 0-2, 0-2
24.  | 25/Sep/2012 |  Csikszereda - [Corona]     |  2-0  | 0-0, 2-0, 0-0
26.  | 25/Sep/2012 |  Ferencvaros - Miskolc      |  2-5  | 1-0, 1-4, 0-1
27.  | 28/Sep/2012 |   Nove Zamky - Miskolc      |  3-1  | 0-1, 1-0, 2-0
28.  | 28/Sep/2012 |  Ferencvaros - Ujpest       |  6-5  | 1-2, 1-1, 3-2, 0-0, 1-0
29.  | 30/Sep/2012 |      Miskolc - Dunaujvaros  |  1-3  | 0-1, 1-0, 0-2
30.  |  1/Oct/2012 |  Ferencvaros - Nove Zamky   |  3-5  | 0-3, 2-2, 1-0
31.  |  2/Oct/2012 |  Dunaujvaros - Nove Zamky   |  3-1  | 1-0, 2-1, 0-0
32.  |  2/Oct/2012 |       Ujpest - Miskolc      |  2-3  | 0-1, 1-0, 1-1, 0-1
34.  |  5/Oct/2012 |   Nove Zamky - [Corona]     |  6-2  | 2-0, 1-1, 3-1
33.  |  5/Oct/2012 |       Ujpest - Csikszereda  |  0-3  | 0-0, 0-3, 0-0
35.  |  5/Oct/2012 |      Miskolc - Ferencvaros  |  3-1  | 0-1, 1-0, 2-0
36.  |  7/Oct/2012 |      Miskolc - Csikszereda  |  3-4  | 1-1, 0-1, 2-2
38.  |  7/Oct/2012 |  Dunaujvaros - Ferencvaros  |  5-4  | 1-0, 2-0, 1-4, 0-0, 1-0
37.  |  7/Oct/2012 |       Ujpest - [Corona]     |  1-2  | 1-0, 0-0, 0-1, 0-0, 0-1
40.  |  8/Oct/2012 |  Ferencvaros - [Corona]     |  4-3  | 0-2, 1-1, 2-0, 0-0, 1-0
41.  |  9/Oct/2012 |  Dunaujvaros - Nove Zamky   |  6-0  | 1-0, 3-0, 2-0
39.  |  9/Oct/2012 |      Miskolc - Ujpest       |  5-3  | 3-0, 2-1, 0-2
43.  | 12/Oct/2012 |   Nove Zamky - Ferencvaros  |  3-2  | 1-1, 2-0, 0-1
44.  | 12/Oct/2012 |  Dunaujvaros - Miskolc      |  2-1  | 1-0, 1-0, 0-1
42.  | 12/Oct/2012 |     [Corona] - Csikszereda  |  2-4  | 0-1, 0-0, 2-3
46.  | 14/Oct/2012 |      Miskolc - Nove Zamky   |  5-4  | 3-2, 1-2, 1-0
47.  | 14/Oct/2012 |       Ujpest - Ferencvaros  |  0-4  | 0-0, 0-1, 0-3
45.  | 14/Oct/2012 |  Csikszereda - Dunaujvaros  |  4-3  | 2-1, 1-0, 0-2, 0-0, 1-0
49.  | 15/Oct/2012 |       Ujpest - Nove Zamky   |  1-4  | 1-1, 0-1, 0-2
48.  | 15/Oct/2012 |     [Corona] - Dunaujvaros  |  1-4  | 0-2, 1-1, 0-1
50.  | 19/Oct/2012 |   Nove Zamky - Dunaujvaros  |  2-5  | 0-2, 1-1, 1-2
51.  | 19/Oct/2012 |  Ferencvaros - Ujpest       |  3-4  | 1-0, 1-3, 1-0, 0-0, 0-1
53.  | 21/Oct/2012 |  Dunaujvaros - Ujpest       |  6-4  | 3-1, 2-2, 1-1
52.  | 21/Oct/2012 |  Ferencvaros - Nove Zamky   |  2-5  | 0-1, 1-3, 1-1
55.  | 22/Oct/2012 |   Nove Zamky - Ujpest       |  2-3  | 2-1, 0-2, 0-0
54.  | 22/Oct/2012 |  Dunaujvaros - Ferencvaros  |  7-5  | 0-3, 4-1, 3-1
58.  | 26/Oct/2012 |   Nove Zamky - Ferencvaros  |  7-4  | 2-1, 3-2, 2-1
57.  | 26/Oct/2012 |  Dunaujvaros - [Corona]     |  5-3  | 1-1, 4-1, 0-1
56.  | 26/Oct/2012 |      Miskolc - Csikszereda  | 14-1  | 4-1, 5-0, 5-0
60.  | 28/Oct/2012 |      Miskolc - [Corona]     |  4-5  | 1-3, 2-0, 1-2
61.  | 28/Oct/2012 |       Ujpest - Dunaujvaros  |  0-10 | 0-2, 0-3, 0-5
59.  | 28/Oct/2012 |  Ferencvaros - Csikszereda  |  4-3  | 0-1, 1-2, 2-0, 1-0
62.  | 29/Oct/2012 |  Dunaujvaros - Csikszereda  |  6-0  | 1-0, 2-0, 3-0
63.  | 30/Oct/2012 |       Ujpest - Ferencvaros  |  2-4  | 1-1, 0-2, 1-1
64.  | 30/Oct/2012 |      Miskolc - Nove Zamky   |  5-4  | 0-2, 3-0, 2-2
65.  |  2/Nov/2012 |  Csikszereda - Miskolc      |  6-1  | 1-0, 2-1, 3-0
67.  |  2/Nov/2012 |  Dunaujvaros - Ujpest       |  5-1  | 3-1, 2-0, 0-0
66.  |  2/Nov/2012 |     [Corona] - Nove Zamky   |  2-5  | 1-1, 0-2, 1-2
68.  |  4/Nov/2012 |  Csikszereda - Nove Zamky   |  2-1  | 2-0, 0-1, 0-0
69.  |  4/Nov/2012 |     [Corona] - Miskolc      |  3-5  | 1-0, 1-2, 1-3
70.  |  4/Nov/2012 |  Ferencvaros - Ujpest       |  2-0  | 1-0, 0-0, 1-0
72.  | 16/Nov/2012 |   Nove Zamky - [Corona]     |  1-2  | 0-0, 0-1, 1-0, 0-0, 0-1
71.  | 16/Nov/2012 |       Ujpest - Csikszereda  |  2-3  | 1-0, 1-1, 0-1, 0-1
73.  | 16/Nov/2012 |      Miskolc - Dunaujvaros  |  4-1  | 0-0, 2-1, 2-0
75.  | 18/Nov/2012 |       Ujpest - [Corona]     |  3-2  | 1-1, 2-1, 0-0
76.  | 18/Nov/2012 |      Miskolc - Nove Zamky   |  7-3  | 3-1, 1-0, 3-2
74.  | 18/Nov/2012 |  Ferencvaros - Csikszereda  |  2-8  | 1-2, 0-1, 1-5
77.  | 19/Nov/2012 |   Nove Zamky - Csikszereda  |  5-0  | 3-0, 0-0, 2-0
78.  | 19/Nov/2012 |  Ferencvaros - Dunaujvaros  |  0-1  | 0-0, 0-1, 0-0
79.  | 19/Nov/2012 |      Miskolc - Ujpest       |  7-2  | 2-1, 2-1, 3-0
82.  | 22/Nov/2012 |       Ujpest - Ferencvaros  |  2-3  | 0-0, 2-2, 0-0, 0-0, 0-1
80.  | 23/Nov/2012 |  Csikszereda - [Corona]     |  6-3  | 0-1, 2-1, 4-1
81.  | 23/Nov/2012 |  Dunaujvaros - Miskolc      |  4-2  | 1-1, 0-0, 3-1
109. | 25/Nov/2012 |  Csikszereda - Ujpest       |  4-5  | 2-1, 1-1, 1-2, 0-0, 0-1
110. | 25/Nov/2012 |     [Corona] - Nove Zamky   |  5-0  | 1-0, 1-0, 3-0
111. | 25/Nov/2012 |  Ferencvaros - Miskolc      |  1-4  | 0-0, 0-3, 1-1
107. | 26/Nov/2012 |     [Corona] - Ujpest       |  5-1  | 2-0, 2-0, 1-1
108. | 26/Nov/2012 |  Dunaujvaros - Ferencvaros  |  5-1  | 2-1, 1-0, 2-0
106. | 26/Nov/2012 |  Csikszereda - Nove Zamky   |  5-1  | 3-0, 0-1, 2-0
90.  | 30/Nov/2012 |     [Corona] - Ferencvaros  |  2-1  | 2-1, 0-0, 0-0
11.  | 30/Nov/2012 |  Dunaujvaros - Ujpest       |  5-3  | 1-1, 2-1, 2-1
89.  | 30/Nov/2012 |  Csikszereda - Miskolc      |  3-2  | 1-2, 0-0, 1-0, 0-0, 1-0
92.  |  2/Dec/2012 |  Csikszereda - Ferencvaros  |  4-1  | 2-0, 1-0, 1-1
93.  |  2/Dec/2012 |     [Corona] - Miskolc      |  6-2  | 3-0, 2-2, 1-0
94.  |  3/Dec/2012 |  Dunaujvaros - Nove Zamky   |  6-1  | 1-1, 3-0, 2-0
96.  |  4/Dec/2012 |   Nove Zamky - Ujpest       |  4-3  | 2-1, 1-2, 0-0, 0-0, 1-0
95.  |  4/Dec/2012 |     [Corona] - Csikszereda  |  5-2  | 3-0, 1-1, 1-1
97.  |  7/Dec/2012 |   Nove Zamky - Csikszereda  |  4-2  | 1-0, 2-1, 1-1
98.  |  7/Dec/2012 |  Dunaujvaros - [Corona]     |  6-3  | 1-1, 3-1, 2-1
99.  |  7/Dec/2012 |  Ferencvaros - Miskolc      |  2-1  | 0-0, 2-0, 0-1
100. |  9/Dec/2012 |      Miskolc - Csikszereda  |  4-2  | 1-1, 2-1, 1-0
101. |  9/Dec/2012 |  Ferencvaros - [Corona]     |  3-6  | 1-3, 1-1, 1-2
102. | 10/Dec/2012 |       Ujpest - Nove Zamky   |  2-6  | 0-1, 1-1, 1-4
103. | 18/Dec/2012 |  Csikszereda - [Corona]     |  2-3  | 1-0, 0-1, 1-1, 0-0, 0-1
104. | 21/Dec/2012 |   Nove Zamky - Ferencvaros  |  5-4  | 2-2, 2-2, 0-0, 0-0, 1-0
105. | 21/Dec/2012 |      Miskolc - Dunaujvaros  |  5-6  | 3-1, 1-3, 1-1, 0-1
86.  | 28/Dec/2012 |  Csikszereda - Ujpest       |  5-1  | 3-0, 2-1, 0-0
88.  | 28/Dec/2012 |   Nove Zamky - Miskolc      |  1-6  | 0-2, 1-3, 0-1
87.  | 28/Dec/2012 |     [Corona] - Dunaujvaros  |  2-1  | 0-1, 0-0, 1-0, 0-0, 1-0
83.  | 30/Dec/2012 |  Csikszereda - Dunaujvaros  |  3-1  | 2-0, 1-0, 0-1
85.  | 30/Dec/2012 |  Ferencvaros - Nove Zamky   |  1-2  | 1-1, 0-1, 0-0
84.  | 30/Dec/2012 |     [Corona] - Ujpest       |  5-2  | 2-0, 2-1, 1-1
112. |  4/Jan/2013 |   Nove Zamky - Ferencvaros  |  6-3  | 0-3, 3-0, 3-0
114. |  4/Jan/2013 |  Dunaujvaros - Miskolc      |  4-3  | 0-0, 2-2, 1-1, 0-0, 1-0
113. |  4/Jan/2013 |       Ujpest - [Corona]     |  0-8  | 0-1, 0-3, 0-4
116. |  6/Jan/2013 |      Miskolc - [Corona]     |  4-5  | 3-3, 1-1, 0-0, 0-1
115. |  6/Jan/2013 |       Ujpest - Csikszereda  |  2-5  | 0-4, 0-1, 2-0
117. |  6/Jan/2013 |  Ferencvaros - Dunaujvaros  |  3-4  | 0-1, 2-1, 1-2
119. |  7/Jan/2013 |   Nove Zamky - [Corona]     |  6-2  | 2-1, 2-0, 2-1
118. |  7/Jan/2013 |  Dunaujvaros - Csikszereda  |  5-2  | 2-1, 1-0, 2-1
120. |  7/Jan/2013 |      Miskolc - Ferencvaros  |  5-2  | 1-0, 2-0, 2-2
123. | 11/Jan/2013 |   Nove Zamky - Ujpest       |  4-1  | 0-1, 2-0, 2-0
122. | 11/Jan/2013 |      Miskolc - Dunaujvaros  |  6-2  | 4-1, 1-1, 1-0
121. | 11/Jan/2013 |     [Corona] - Csikszereda  |  0-2  | 0-1, 0-0, 0-1
126. | 13/Jan/2013 |  Dunaujvaros - Nove Zamky   |  2-1  | 0-0, 0-1, 2-0
125. | 13/Jan/2013 |       Ujpest - Miskolc      |  1-9  | 1-3, 0-3, 0-3
124. | 13/Jan/2013 |     [Corona] - Ferencvaros  |  7-1  | 0-1, 4-0, 3-0
127. | 14/Jan/2013 |  Csikszereda - Ferencvaros  |  6-1  | 2-0, 1-0, 3-1
129. | 15/Jan/2013 |       Ujpest - Dunaujvaros  |  1-7  | 1-2, 0-5, 0-0
130. | 18/Jan/2013 |  Csikszereda - Dunaujvaros  |  3-2  | 0-0, 2-1, 1-1
132. | 18/Jan/2013 |      Miskolc - Ujpest       |  9-2  | 1-0, 4-2, 4-0
131. | 18/Jan/2013 |     [Corona] - Nove Zamky   |  4-0  | 3-0, 0-0, 1-0
------------------------------------------------------------------------------------
====================================================================================
