unit Mol2pas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.jpeg, Vcl.ExtCtrls,
  Vcl.StdCtrls, ClipBrd, XML.VerySimple, PNGImage, Vcl.Menus, Vcl.ImgList, ShellAPI;

const nCol=10;

type
  TF1 = class(TForm)
    mapImg: TImage;
    BalloonHint1: TBalloonHint;
    SB: TScrollBox;
    Label1: TLabel;
    Panel1: TPanel;
    exitB: TButton;
    logL: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Panel2: TPanel;
    Label7: TLabel;
    l1: TLabel;
    l2: TLabel;
    l4: TLabel;
    l3: TLabel;
    l7: TLabel;
    l8: TLabel;
    l6: TLabel;
    l5: TLabel;
    l9: TLabel;
    l10: TLabel;
    ll1: TLabel;
    ll2: TLabel;
    ll3: TLabel;
    ll4: TLabel;
    ll5: TLabel;
    ll6: TLabel;
    ll7: TLabel;
    ll8: TLabel;
    ll9: TLabel;
    ll10: TLabel;
    colL1: TLabel;
    colL2: TLabel;
    colL3: TLabel;
    sh1: TShape;
    ImageList1: TImageList;
    PM1: TPopupMenu;
    About1: TMenuItem;
    Settings1: TMenuItem;
    Exit1: TMenuItem;
    Updatedata1: TMenuItem;
    Label8: TLabel;
    Label9: TLabel;
    eamstats1: TMenuItem;
    menuSh: TShape;
    menuImg: TImage;
    ExporttoTXT1: TMenuItem;
    More1: TMenuItem;
    ExportattendanceXML1: TMenuItem;
    SD1: TSaveDialog;
    Opendatafolder1: TMenuItem;
    Stats1: TMenuItem;
    eamevolutionstats1: TMenuItem;
    Seasonsomekindofstats1: TMenuItem;
    Standingsevolution1: TMenuItem;
    procedure EnablePNGForImage(im: TImage);
    procedure CreateMainFormElements;
    procedure RecreateRepositionAndRefreshResults;
    procedure RefreshStandings;
    procedure TeamImgClick(Sender: TObject);
    procedure TeamImgMouseEnter(Sender: TObject);
    procedure TeamImgMouseLeave(Sender: TObject);
    procedure StLabelMouseEnter(Sender: TObject);
    procedure StLabelMouseLeave(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Settings1Click(Sender: TObject);
    procedure menuImgClick(Sender: TObject);
    procedure Updatedata1Click(Sender: TObject);
    procedure eamstats1Click(Sender: TObject);
    procedure TeamMatchesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure ExporttoTXT1Click(Sender: TObject);
    procedure ExportattendanceXML1Click(Sender: TObject);
    procedure Opendatafolder1Click(Sender: TObject);
    procedure eamevolutionstats1Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure Standingsevolution1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TResultLabelSet=record
  timeL, homeTL, awayTL, scoreL: TLabel;
end;

type TResultLabels=record
  dayL: TLabel;
  nResults: word;
  resLSet: array of TResultLabelSet;
end;

var
  F1: TF1;
  timg, stImg: array of TImage;
  colWr: array[0..nCol-1] of real; //standings labels width (%)
  stT: array[0..nCol-1] of array of TLabel;
  rl: array of TResultLabels;
  lastTop: word;

implementation

{$R *.dfm}

uses DataTypes, functii, Settings, Updater, Team, AttendanceUpdater, TeamEvolution, StandingsEvolution,
  About;

procedure TF1.eamevolutionstats1Click(Sender: TObject);
begin FTeamEvol.Show; FTeamEvol.timgClick(FTeamEvol.FindComponent('img'+Sett.Find('FavoriteTeam').Attribute['ID'])) end;

procedure TF1.eamstats1Click(Sender: TObject);
begin FTeamToShowX:=-1; if FTeam.Showing then FTeam.OnShow(eamstats1) else FTeam.Show end;

procedure TF1.exitBClick(Sender: TObject);
begin
  Halt
end;

procedure TF1.ExportattendanceXML1Click(Sender: TObject);
begin FAttUpd.Show end;

procedure TF1.ExporttoTXT1Click(Sender: TObject);
  {*}function fs(s: string; n: word): string; var i: word;
  begin if s=favT then s:='['+s+']'; for i:=length(s) to n do s:=s+' '; result:=s end;
  {*}function fs2(s: string; n: word): string; var i: word;
  begin if s=favT then s:='['+s+']'; for i:=length(s) to n do s:=' '+s; result:=s end;
const x1='====================================================================================';
      x2='------------------------------------------------------------------------------------';
var fn, xs: string; i, maxIDL: word;
begin
  fn:='data\exports\export '+formatdatetime('yyyy-mm-dd', now)+'.txt';
  assignfile(f, fn); rewrite(f);
  maxIDL:=0; for i:=0 to nt-1 do if length(st[i].TeamID)>maxIDL then maxIDL:=length(st[i].TeamID);
  write(f, x1, nl, '      LIGA MOL by BogdanBBA', nl, '      Exported '+formatdatetime('dddd, d mmmm yyyy, "at" h:nn', now), nl, x1, nl, x2, nl);
  xs:='    '+fs('Team', maxIDL)+' MP | 3p  2p  1p  0p |     Goals     |  Pts';
  write(f, '    STANDINGS', nl, x2, nl, xs, nl, x2, nl);
  for i:=0 to nt-1 do
    begin
      write(f, i+1, '. ', fs(st[i].TeamID, maxIDL+1));
      write(f, st[i].M:3, ' |', st[i].p3:3, st[i].p2:4, st[i].p1:4, st[i].p0:4, ' |');
      write(f, fs2(inttostr(st[i].GF)+'-'+inttostr(st[i].GA), 7), fs2(plusminus(st[i].GDiff), 5), ' |', st[i].Pts:4);
      writeln(f)
    end;
  xs:=' ID  |       Date        |      Against | Score |    Periods';
  write(f, x2, dnl, x2, nl, '    Home matches for ', ansiuppercase(favT), nl, x2, nl, xs, nl, x2, nl);
  for i:=0 to nr-1 do
    begin
      if rs[i].HomeT=favT then
        begin
          write(f, fs(rs[i].ID+'. ', 4), '|', fs2(formatdatetime('d mmmm yyyy', rs[i].When), 17), ' | ');
          write(f, fs2(rs[i].AwayT, maxIDL), ' | ');
          if rs[i].Played then write(f, fs2(inttostr(rs[i].ResultHG), 1), '-', fs(inttostr(rs[i].ResultAG), 1), ' | ', FormatPeriodScores(i))
          else write(f, '  -   | ---');
          writeln(f)
        end;
    end;
  xs:=' ID  |   Date      |         Away - Home         | Score |    Periods';
  write(f, x2, dnl, x2, nl, '    RESULTS', nl, x2, nl, xs, nl, x2, nl);
  for i:=0 to nr-1 do
    begin
      if rs[i].Played then
        begin
          write(f, fs(rs[i].ID+'. ', 4), '|', fs2(formatdatetime('d/mmm/yyyy', rs[i].When), 11), ' | ');
          write(f, fs2(rs[i].HomeT, maxIDL), ' - ', fs(rs[i].AwayT, maxIDL), ' | ');
          write(f, fs2(inttostr(rs[i].ResultHG), 1), '-', fs(inttostr(rs[i].ResultAG), 1), ' | ', FormatPeriodScores(i));
          writeln(f)
        end;
    end;
  write(f, x2, nl, x1, nl);
  closefile(f); ShellExecute(Handle, 'open', PChar(fn), nil, nil, SW_SHOW)
end;

procedure TF1.EnablePNGForImage(im: TImage);
begin im.Picture.RegisterFileFormat('PNG', 'Portable Network Graphics', TPNGImage) end;

procedure TF1.RecreateRepositionAndRefreshResults;
  {*}function CountResultsOnDay(day: TDate; addThemToList: TStringList): word;
  var i, r: word;
  begin
    r:=0; addThemToList.Clear;
    for i:=0 to nr-1 do if rs[i].Played then if trunc(rs[i].When)=day then begin inc(r); addThemToList.Add(rs[i].ID) end; result:=r
  end;
  {*}procedure CopyLabelFormattingAndPositioning(sL: TLabel; var dL: TLabel);
  begin
    if sl=nil then MessageDlg('RecreateRepositionAndRefreshResults.CopyLabelFormattingAndPositioning() ERROR: source label = nil', mtError, [mbIgnore], 0);
    if dl=nil then begin dl:=TLabel.Create(Self); dl.Parent:=F1.SB end;
    dl.Font:=sl.Font; dl.AutoSize:=sl.AutoSize; dl.Left:=sl.Left; dl.Width:=sl.Width;
    dl.Alignment:=sl.Alignment; dl.Top:=lastTop; dl.OnClick:=sl.OnClick
  end;
var i, j, k, nd: word; xd: TDate; dl, ml: TStringList;
begin
  dl:=TStringList.Create; dl.Sorted:=true; dl.Duplicates:=dupIgnore; ml:=TStringList.Create; lastTop:=3;
  for i:=0 to nr-1 do if rs[i].Played then dl.Add(formatdatetime('yyyy/mm/dd', rs[i].When));
  nd:=dl.Count; setlength(rl, nd);
  for i:=0 to nd-1 do
    begin
      xd:=GetDate(dl[i]); rl[i].nResults:=CountResultsOnDay(xd, ml); rl[i].nResults:=ml.Count; setlength(rl[i].resLSet, rl[i].nResults);
      CopyLabelFormattingAndPositioning(Label2, rl[i].dayL); rl[i].dayL.Hint:='Match day '+inttostr(i+1);
      rl[i].dayL.Caption:=formatdatetime('dddd, d mmmm yyyy', xd); inc(lastTop, 27);
      for j:=0 to rl[i].nResults-1 do
        begin
          k:=ResultPosByID(ml[j]);
          CopyLabelFormattingAndPositioning(Label3, rl[i].resLSet[j].timeL);  rl[i].resLSet[j].timeL.Caption:=formatdatetime('h:nn', rs[k].When);
          CopyLabelFormattingAndPositioning(Label4, rl[i].resLSet[j].homeTL); rl[i].resLSet[j].homeTL.Caption:=rs[k].HomeT;
          CopyLabelFormattingAndPositioning(Label5, rl[i].resLSet[j].awayTL); rl[i].resLSet[j].awayTL.Caption:=rs[k].AwayT;
          CopyLabelFormattingAndPositioning(Label6, rl[i].resLSet[j].scoreL);
          with rl[i].resLSet[j].scoreL do begin Caption:=FormatScore(k); Hint:=FormatPeriodScores(k)+'|'+ScoreSubtitle(k) end;
          inc(lastTop, 20+7*integer(j=rl[i].nResults-1))
        end
    end
end;

procedure TF1.About1Click(Sender: TObject);
begin FAbout.ShowModal end;

procedure TF1.CreateMainFormElements;
//teams and results are known
  {*}procedure CopyLabelFormatting(sL: TLabel; var dL: TLabel);
  begin
    if sl=nil then MessageDlg('CreateMainFormElements.CopyLabelFormatting() ERROR: source label = nil', mtError, [mbIgnore], 0);
    if dl=nil then begin dl:=TLabel.Create(Self); dl.Parent:=F1.Panel2 end;
    dl.Font:=sl.Font; dl.AutoSize:=sl.AutoSize;  dl.Alignment:=sl.Alignment
  end;
var i, j: word;
begin
  mapImg.Picture.LoadFromFile('data\img\harta2.png');
  setlength(timg, nt);
  for i:=0 to nt-1 do
    begin
      timg[i]:=TImage.Create(Self); timg[i].Parent:=F1; EnablePNGForImage(timg[i]); timg[i].Name:='img'+LettersAndDigits(T.ChildNodes[i].Attribute['ID']);
      timg[i].AutoSize:=true;
      try timg[i].Picture.LoadFromFile('data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png') except MessageDlg('TF1.CreateMainFormElements: Failed to load team image file "data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png".', mtError, [mbIgnore], 0) end;
      timg[i].Cursor:=crHandPoint; timg[i].OnClick:=TeamImgClick; timg[i].OnMouseEnter:=TeamImgMouseEnter; timg[i].OnMouseLeave:=TeamImgMouseLeave;
      timg[i].Hint:=t.ChildNodes[i].Attribute['fullName']+'|'+t.ChildNodes[i].Attribute['city'];
    end;
  //
  j:=Panel2.Width-(nCol-1)*6;
  for i:=0 to nCol-1 do colWr[i]:=TLabel(FindComponent('l'+inttostr(i+1))).Width/j;
  for i:=0 to nt-1 do
    begin
      setlength(stT[i], nCol);
      for j:=0 to nCol-1 do
        begin
          CopyLabelFormatting(TLabel(FindComponent('ll'+inttostr(j+1))), stT[i,j]);
          if j=0 then
            begin
              stT[i,j].Caption:=inttostr(i+1)+'.';
              with stT[i,j].Font do case i of 0..3: Color:=colL2.Font.Color; 4..6: Color:=colL3.Font.Color; else Color:=clBlack end
            end;
          if j in [1, 3..6] then with stT[i,j] do begin Cursor:=crHandPoint; OnMouseEnter:=StLabelMouseEnter; OnMouseLeave:=StLabelMouseLeave end;
          if j=1 then stT[i,j].OnClick:=TeamImgClick;
          if j in [3..6] then stT[i,j].OnClick:=TeamMatchesClick
        end
    end;
  //
  RecreateRepositionAndRefreshResults;
  RefreshStandings
end;

procedure TF1.FormCreate(Sender: TObject);
begin
  InitializeAndReadData;
  CreateMainFormElements
end;

procedure TF1.FormResize(Sender: TObject);
  {*}procedure PlaceLabelsOATO(leftL, rightL: TLabel; withDiffPx: word);
  begin rightL.Left:=leftL.Left+leftL.Width+withDiffPx end;
const scoreLW=55;
var w, h, x, y, i, j: word; r: real;
begin
  w:=F1.Width; h:=F1.Height;
  mapImg.Width:=w-400; mapImg.Height:=round(mapImg.Width/(mapImg.Picture.Width/mapImg.Picture.Height));
  menuImg.Top:=mapImg.Height-menuImg.Height; menuSh.Top:=menuImg.Top;
  r:=mapImg.Width/strtoint(Sett.Find('CoordForImageMaxWidth').Attribute['value']);
  for i:=0 to nt-1 do
    begin
      DecodePair(t.ChildNodes[i].Attribute['mapXY'], ',', x, y);
      timg[i].Left:=round(r*(x-timg[i].Width div 2)); timg[i].Top:=round(r*(y-timg[i].Height div 2))
    end;
  //
  SB.Left:=mapImg.Width; SB.Width:=w-SB.Left; Label1.Left:=SB.Left+12; Panel1.Left:=SB.Left; Panel1.Width:=SB.Width;
  Panel1.Height:=70; Panel1.Top:=h-Panel1.Height; SB.Height:=h-SB.Top-Panel1.Height;
  exitB.Left:=Panel1.Width-exitB.Width-18; logL.Left:=Panel1.Width-logL.Width-18;
  Panel2.Top:=Label7.Top+Label7.Height+6; Panel2.Width:=mapImg.Width; Panel2.Height:=h-mapImg.Height;
  //
  x:=(SB.Width-Label3.Width-3*5-scoreLW) div 2;
  Label4.Left:=Label3.Width+3*2; Label4.Width:=x;
  Label6.Left:=Label4.Left+Label4.Width+3; Label6.Width:=scoreLW;
  Label5.Left:=Label6.Left+Label6.Width+3; Label5.Width:=x;
  //
  x:=(Panel2.Height-l1.Height-6) div (nt+2);
  for i:=0 to nCol-1 do
    begin
      TLabel(FindComponent('l'+inttostr(i+1))).Width:=round((Panel2.Width-(nCol-1)*6)*colWr[i]);
      if i>0 then PlaceLabelsOATO(TLabel(FindComponent('l'+inttostr(i))), TLabel(FindComponent('l'+inttostr(i+1))), 6)
    end;
  for i:=0 to nt-1 do
    begin
      for j:=0 to nCol-1 do
        begin
          with TLabel(FindComponent('l'+inttostr(j+1))) do begin stT[i,j].Left:=Left; stT[i,j].Width:=Width end;
          stT[i,j].Top:=l1.Height+6+i*x; stT[i,j].Height:=ll1.Height;
        end;
    end;
end;

procedure TF1.FormShow(Sender: TObject);
var xs: string;
begin
  if Sett.Find('ShowHints')<>nil then xs:=Sett.Find('ShowHints').Attribute['value'] else xs:='true';
  F1.ShowHint:=truefalsetoboolean(xs, true)
end;

procedure TF1.Label4Click(Sender: TObject);
var x, i: shortint;
begin
  x:=-1; for i:=0 to nt-1 do if TLabel(Sender).Caption=T.ChildNodes[i].Attribute['ID'] then begin x:=i; break end;
  if x<>-1 then begin FTeamToShowX:=x; if FTeam.Showing then FTeam.OnShow(Label4) else FTeam.Show end
end;

procedure TF1.Label7Click(Sender: TObject);
//var i: word;
begin
	//for i:=low(timg) to high(timg) do timg[i].Visible:=not timg[i].Visible
end;

procedure TF1.menuImgClick(Sender: TObject);
var p: TPoint;
begin getcursorpos(p); PM1.Popup(p.X, p.Y) end;

procedure TF1.Opendatafolder1Click(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar(GetCurrentDir+'\data\'), nil, nil, SW_SHOW)
end;

procedure TF1.RefreshStandings;
  {*}procedure ProcessResultForStandings(r: TResult);
  var i, j: integer;
  begin
    i:=StandingsPosForTeam(r.HomeT); j:=StandingsPosForTeam(r.AwayT);
    if (i=-1) or (j=-1) or ((r.ResultHG=r.ResultAG) and (not r.ForceResult.Force)) then exit;
    case NResultPoints(r, r.HomeT) of
    3: begin inc(st[i].p3); inc(st[j].p0) end;
    2: begin inc(st[i].p2); inc(st[j].p1) end;
    1: begin inc(st[i].p1); inc(st[j].p2) end;
    0: begin inc(st[i].p0); inc(st[j].p3) end
    end;
    with st[i] do begin inc(GF, r.ResultHG); inc(GA, r.ResultAG); M:=p3+p2+p1+p0; GDiff:=GF-GA; Pts:=3*p3+2*p2+p1 end;
    with st[j] do begin inc(GF, r.ResultAG); inc(GA, r.ResultHG); M:=p3+p2+p1+p0; GDiff:=GF-GA; Pts:=3*p3+2*p2+p1 end
//    with st[i] do
//      begin
//        if r.nPeriods=3 then begin inc(p3, integer(r.ResultHG>r.ResultAG)); inc(p0, integer(r.ResultHG<r.ResultAG)) end
//        else begin inc(p2, integer(r.ResultHG>r.ResultAG)); inc(p1, integer(r.ResultHG<r.ResultAG)) end;
//        inc(GF, r.ResultHG); inc(GA, r.ResultAG); M:=p3+p2+p1+p0; GDiff:=GF-GA; Pts:=3*p3+2*p2+p1
//      end;
//    with st[j] do
//      begin
//        if r.nPeriods=3 then begin inc(p3, integer(r.ResultAG>r.ResultHG)); inc(p0, integer(r.ResultAG<r.ResultHG)) end
//        else begin inc(p2, integer(r.ResultAG>r.ResultHG)); inc(p1, integer(r.ResultAG<r.ResultHG)) end;
//        inc(GF, r.ResultAG); inc(GA, r.ResultHG); M:=p3+p2+p1+p0; GDiff:=GF-GA; Pts:=3*p3+2*p2+p1
//      end;
  end;
  {*}function TeamsMustBeReversed(a, b: TStandingsPosition): boolean;
  begin
    if a.Pts<>b.Pts then result:=a.Pts<b.Pts
    else if a.GDiff<>b.GDiff then result:=a.GDiff<b.GDiff
    else if a.GF<>b.GF then result:=a.GF<b.GF
    else if a.GA<>b.GA then result:=a.GA<b.GA
    else result:=false
  end;
var i, j: word; auxTName: string; aux: TStandingsPosition;
begin
  //refresh standings
  for i:=0 to nt-1 do
    with st[i] do
      begin
        TeamID:=T.ChildNodes[i].Attribute['ID'];
        M:=0; p3:=0; p2:=0; p1:=0; p0:=0; GF:=0; GA:=0; GDiff:=0; Pts:=0
      end;
  for i:=0 to nr-1 do if rs[i].Played then ProcessResultForStandings(rs[i]);
  for i:=0 to nt-2 do for j:=i+1 to nt-1 do
    if TeamsMustBeReversed(st[i], st[j]) then
      begin aux:=st[i]; st[i]:=st[j]; st[j]:=aux end;
  //refresh table
  for i:=0 to nt-1 do
    begin
    	try
      	if TrueFalseToBoolean(Sett.Find('ShowFullTeamNameInStandings').Attribute['value'], true) then
        	begin
	        	auxTName := T.Find('team', 'ID', st[i].TeamID).Attribute['fullName'];
            stT[i,1].Font.Style:=[]
          end
        else
        	begin
	        	auxTName := T.Find('team', 'ID', st[i].TeamID).Attribute['ID'];
            stT[i,1].Font.Style:=[fsBold]
          end
      except auxTName := 'Error' end;
      stT[i,1].Caption := auxTName;
      stT[i,2].Caption := inttostr(st[i].M);
      stT[i,3].Caption := inttostr(st[i].p3);
      stT[i,4].Caption := inttostr(st[i].p2);
      stT[i,5].Caption := inttostr(st[i].p1);
      stT[i,6].Caption := inttostr(st[i].p0);
      stT[i,7].Caption := inttostr(st[i].GF)+'-'+inttostr(st[i].GA);
      stT[i,8].Caption := PlusMinus(st[i].GDiff);
      with stT[i,8].Font do if st[i].GDiff=0 then Color:=clWhite else if st[i].GDiff<0 then Color:=Label9.Font.Color else Color:=Label8.Font.Color;
      stT[i,9].Caption := inttostr(st[i].Pts)
    end
end;

procedure TF1.TeamMatchesClick(Sender: TObject);
var x, y, i, j: integer;
begin
  x:=-1; y:=-1; for i:=0 to nt-1 do for j:=3 to 6 do if Sender=stT[i,j] then begin x:=i; y:=j; break end;
  if (x<>-1) and (y<>-1) then
    begin
      TeamImgClick(stT[x,1]); FTeam.pSt1Click(FTeam.pSt2); FTeam.pV1Click(FTeam.pV1); FTeam.pR1Click(FTeam.FindComponent('pR'+inttostr(y+1)))
    end
end;

procedure TF1.TeamImgClick(Sender: TObject);
var x, xx: integer;
begin
  xx:=-1;
  if Sender is TImage then begin for x:=0 to nt-1 do if timg[x]=Sender then begin xx:=x; break end end
  else if Sender is TLabel then begin for x:=0 to nt-1 do if (T.ChildNodes[x].Attribute['fullName']=TLabel(Sender).Caption) or (T.ChildNodes[x].Attribute['ID']=TLabel(Sender).Caption) then begin xx:=x; break end end;
  if xx<>-1 then begin FTeamToShowX:=xx; if FTeam.Showing then FTeam.OnShow(Sender) else FTeam.Show end
end;

procedure TF1.TeamImgMouseEnter(Sender: TObject);
var x: word;
begin
  for x:=0 to nt-1 do with stT[StandingsPosForTeam(T.ChildNodes[x].Attribute['ID']),1].Font do if timg[x]=Sender then
    begin Color:=clYellow end else begin Color:=clWhite end
end;

procedure TF1.TeamImgMouseLeave(Sender: TObject);
var x: word;
begin
  for x:=0 to nt-1 do with stT[x,1].Font do begin Color:=clWhite end
end;

procedure TF1.Updatedata1Click(Sender: TObject);
begin FUpdate.Show end;

procedure TF1.Settings1Click(Sender: TObject);
begin if FSett.showing then FSett.OnShow(F1) else FSett.Show end;

procedure TF1.Standingsevolution1Click(Sender: TObject);
begin
	FStandsingsEvol.Show
end;

procedure TF1.StLabelMouseEnter(Sender: TObject);
var x, i: integer; a, b: word;
begin
  TLabel(Sender).Font.Color:=clYellow;
  x:=-1; for i:=0 to nt-1 do if stT[i,1]=Sender then begin x:=i; break end;
  if x<>-1 then
    begin
      with TImage(FindComponent('img'+LettersAndDigits(st[x].TeamID))) do begin a:=Left+Width div 2; b:=Top+Height div 2 end;
      with sh1 do begin Brush.Color:=GetTeamColor(st[x].TeamID, true); Pen.Color:=GetTeamColor(st[x].TeamID, false); Left:=a-Width div 2; Top:=b-Height div 2; Visible:=true end
    end
end;

procedure TF1.StLabelMouseLeave(Sender: TObject);
begin
  TLabel(Sender).Font.Color:=clWhite; sh1.Visible:=false
end;

end.
