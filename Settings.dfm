object FSett: TFSett
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Settings'
  ClientHeight = 262
  ClientWidth = 347
  Color = 4209709
  Font.Charset = ANSI_CHARSET
  Font.Color = clSilver
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object Label7: TLabel
    Left = 16
    Top = 8
    Width = 109
    Height = 34
    Caption = 'Settings'
    Font.Charset = ANSI_CHARSET
    Font.Color = 2861823
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 28
    Top = 56
    Width = 97
    Height = 20
    Caption = 'Favorite team'
  end
  object Label2: TLabel
    Left = 44
    Top = 128
    Width = 79
    Height = 20
    Caption = 'Show hints'
  end
  object Label3: TLabel
    Left = 44
    Top = 152
    Width = 238
    Height = 20
    Caption = 'Show full team name in standings'
  end
  object closeB: TButton
    Left = 175
    Top = 192
    Width = 153
    Height = 49
    Caption = 'Close'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = closeBClick
  end
  object saveB: TButton
    Left = 16
    Top = 192
    Width = 153
    Height = 49
    Caption = 'Save'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = saveBClick
  end
  object cb1: TComboBox
    Left = 28
    Top = 82
    Width = 288
    Height = 28
    Style = csDropDownList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object ch1: TCheckBox
    Left = 28
    Top = 130
    Width = 97
    Height = 17
    TabOrder = 3
  end
  object ch2: TCheckBox
    Left = 28
    Top = 154
    Width = 254
    Height = 17
    TabOrder = 4
  end
  object closeT: TTimer
    Enabled = False
    OnTimer = closeTTimer
    Left = 192
    Top = 224
  end
end
