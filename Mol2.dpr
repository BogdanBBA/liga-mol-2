program Mol2;

uses
  Vcl.Forms,
  Mol2pas in 'Mol2pas.pas' {F1},
  Settings in 'Settings.pas' {FSett},
  Updater in 'Updater.pas' {FUpdate},
  Team in 'Team.pas' {FTeam},
  AttendanceUpdater in 'AttendanceUpdater.pas' {FAttUpd},
  TeamEvolution in 'TeamEvolution.pas' {FTeamEvol},
  StandingsEvolution in 'StandingsEvolution.pas' {FStandsingsEvol},
  About in 'About.pas' {FAbout};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TF1, F1);
  Application.CreateForm(TFSett, FSett);
  Application.CreateForm(TFUpdate, FUpdate);
  Application.CreateForm(TFTeam, FTeam);
  Application.CreateForm(TFAttUpd, FAttUpd);
  Application.CreateForm(TFTeamEvol, FTeamEvol);
  Application.CreateForm(TFStandsingsEvol, FStandsingsEvol);
  Application.CreateForm(TFAbout, FAbout);
  Application.Run;
end.
