unit About;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFAbout = class(TForm)
    Panel1: TPanel;
    Label7: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    closeB: TButton;
    Memo1: TMemo;
    procedure closeBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAbout: TFAbout;

implementation

{$R *.dfm}

procedure TFAbout.closeBClick(Sender: TObject);
begin
	FAbout.Close
end;

end.
