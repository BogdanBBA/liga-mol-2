unit StandingsEvolution;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VCLTee.TeEngine, VCLTee.Series,
  Vcl.ExtCtrls, VCLTee.TeeProcs, VCLTee.Chart, Vcl.StdCtrls, DataTypes, Functii,
  Vcl.ComCtrls;

type
  TFStandsingsEvol = class(TForm)
    closeB: TButton;
    ch: TChart;
    Series1: TBarSeries;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    tb1: TTrackBar;
    Label4: TLabel;
    Shape1: TShape;
    ch1: TCheckBox;
    Label5: TLabel;
    ch2: TCheckBox;
    Label6: TLabel;
    Panel1: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    procedure FormResize(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tb1Change(Sender: TObject);
    procedure ch1Click(Sender: TObject);
    procedure chClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    procedure RefreshChart;
  end;

var
  FStandsingsEvol: TFStandsingsEvol;
  //local pointer!
  localSt: array of TStandingsPosition;
	initTOrder, initTOrderCol: TStringList;
  posRange: array of TValStats;

implementation

{$R *.dfm}

procedure TFStandsingsEvol.ch1Click(Sender: TObject);
begin
  FStandsingsEvol.RefreshChart
end;

procedure TFStandsingsEvol.closeBClick(Sender: TObject);
begin
	FStandsingsEvol.Close
end;

procedure TFStandsingsEvol.FormResize(Sender: TObject);
begin
	ch.Width:=FStandsingsEvol.Width-2*ch.Left; ch.Height:=FStandsingsEvol.Height-ch.Top-ch.Left;
  Panel1.Width:=FStandsingsEvol.Width-Panel1.Left
end;

procedure TFStandsingsEvol.FormShow(Sender: TObject);
begin
	tb1Change(FStandsingsEvol);
	FStandsingsEvol.RefreshChart
end;

procedure TFStandsingsEvol.tb1Change(Sender: TObject);
begin
	Label4.Caption:=Format('Line width: %dpx', [tb1.Position]);
  FStandsingsEvol.RefreshChart
end;

procedure TFStandsingsEvol.RefreshChart;
  {*}function StandingsPosForTeam(ID: string): integer;
  var i: word;
  begin
  	result:=-1;
    for i:=0 to nt-1 do if localSt[i].TeamID=ID then begin result:=i; break end
  end;
  {*}procedure ProcessResultForStandings(r: TResult);
  var i, j: integer;
  begin
    i:=StandingsPosForTeam(r.HomeT); j:=StandingsPosForTeam(r.AwayT); if (i=-1) or (j=-1) or (r.ResultHG=r.ResultAG) then exit;
    with localSt[i] do
      begin
        if r.nPeriods=3 then begin inc(p3, integer(r.ResultHG>r.ResultAG)); inc(p0, integer(r.ResultHG<r.ResultAG)) end
        else begin inc(p2, integer(r.ResultHG>r.ResultAG)); inc(p1, integer(r.ResultHG<r.ResultAG)) end;
        inc(GF, r.ResultHG); inc(GA, r.ResultAG); M:=p3+p2+p1+p0; GDiff:=GF-GA; Pts:=3*p3+2*p2+p1
      end;
    with localSt[j] do
      begin
        if r.nPeriods=3 then begin inc(p3, integer(r.ResultAG>r.ResultHG)); inc(p0, integer(r.ResultAG<r.ResultHG)) end
        else begin inc(p2, integer(r.ResultAG>r.ResultHG)); inc(p1, integer(r.ResultAG<r.ResultHG)) end;
        inc(GF, r.ResultAG); inc(GA, r.ResultHG); M:=p3+p2+p1+p0; GDiff:=GF-GA; Pts:=3*p3+2*p2+p1
      end;
  end;
  {*}function TeamsMustBeReversed(a, b: TStandingsPosition): boolean;
  begin
    if a.Pts<>b.Pts then result:=a.Pts<b.Pts
    else if a.GDiff<>b.GDiff then result:=a.GDiff<b.GDiff
    else if a.GF<>b.GF then result:=a.GF<b.GF
    else if a.GA<>b.GA then result:=a.GA<b.GA
    else result:=false
  end;

var LS: array of TLineSeries;
		i, j, k, k2, nd, p: word; xs: string; xd: TDate; dl: TStringList; aux: TStandingsPosition;

begin
	initTOrder:=TStringList.Create; initTOrder.Sorted:=false; initTOrderCol:=TStringList.Create; initTOrderCol.Sorted:=false;
  SetLength(posRange, nt); for i:=0 to nt-1 do with posRange[i] do begin min:=high(word); max:=low(word); count:=0; total:=0; avg:=0 end;

  SetLength(localSt, nt);
  for i:=0 to nt-1 do
    with localSt[i] do
      begin
        xs:=T.ChildNodes[i].Attribute['colors']; xs:=Copy(xs, 1, pos(',', xs)-1);
        TeamID:=T.ChildNodes[i].Attribute['ID'];
        initTOrder.Add(TeamID); initTOrderCol.Add(xs);
        M:=0; p3:=0; p2:=0; p1:=0; p0:=0; GF:=0; GA:=0; GDiff:=0; Pts:=0
      end;

  SetLength(LS, nt);
  for i:=0 to nt-1 do
  	begin
    	LS[i]:=TLineSeries.Create(Self);
      LS[i].Title:=localSt[i].TeamID;
      LS[i].Color:=HtmlToColor(initTOrderCol[i]);
      LS[i].Pen.Width:=tb1.Position;
    end;

  dl:=TStringList.Create; dl.Sorted:=true; dl.Duplicates:=dupIgnore;
  for i:=0 to nr-1 do if rs[i].Played then dl.Add(formatdatetime('yyyy/mm/dd', rs[i].When));
  nd:=dl.Count;

  for j := 0 to nt - 1 do
		for i := 0 to nd div 32 do
			LS[j].AddY(StandingsPosForTeam(initTOrder[j]) + 1, '', HtmlToColor(initTOrderCol[j]));

  for i:=0 to nd-1 do
    begin
      xd:=GetDate(dl[i]);
      for j:=0 to nr-1 do
      	if Trunc(rs[j].When)=Trunc(xd) then
        	begin
            ProcessResultForStandings(rs[j]);
            for k:=0 to nt-2 do for k2:=k+1 to nt-1 do
              if TeamsMustBeReversed(localSt[k], localSt[k2]) then
                begin aux:=localSt[k]; localSt[k]:=localSt[k2]; localSt[k2]:=aux end;
          end;
      //xs2:='standings after match day processing:'+dnl; for k:=0 to nt-1 do xs2:=format('%s%d. %s%s', [xs2, k+1, localSt[k].TeamID, nl]); showmessage(xs2);

      for j:=0 to nt-1 do
      	begin
        	with posRange[initTOrder.IndexOf(localSt[j].TeamID)] do
		      	begin
            	p:=StandingsPosForTeam(localSt[j].TeamID)+1;
            	if p<min then min:=p;
              if p>max then max:=p;
              inc(total, p);
              inc(count);
              avg:=total/count
    		    end;
	      	LS[j].AddY(StandingsPosForTeam(initTOrder[j])+1, FormatDateTime('d-mmm-yy', xd), HtmlToColor(initTOrderCol[j]));
        end
    end;

  ch.ClearChart; ch.View3D:=false;
  ch.BottomAxis.Grid.Visible:=ch1.Checked; ch.LeftAxis.Grid.Visible:=ch2.Checked;
  ch.LeftAxis.SetMinMax(0, nt+1); ch.LeftAxis.Increment:=1;
  ch.LeftAxis.Automatic:=false; ch.LeftAxis.Inverted:=true;
  ch.LeftAxis.LabelsFont.Assign(Label1.Font);
  ch.BottomAxis.Minimum:=-5;
  ch.BottomAxis.LabelsFont.Assign(Label2.Font);
  ch.Legend.Font.Assign(Label3.Font);
  for j:=nt-1 downto 0 do
    ch.AddSeries(LS[j]);
  Panel1.Visible:=false
end;

procedure TFStandsingsEvol.chClickSeries(Sender: TCustomChart;
  Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  {*}function StandingsPosForTeam(ID: string): integer;
  var i: word;
  begin
  	result:=-1;
    for i:=0 to nt-1 do if localSt[i].TeamID=ID then begin result:=i; break end
  end;
var i, k: integer;
begin
	for i:=0 to ch.SeriesCount-1 do
		if Series=ch.Series[i] then
    	begin
      	k:=nt-i-1;
        with localSt[StandingsPosForTeam(initTOrder[k])] do
        	begin
          	Label20.Caption:=T.Find('team', 'ID', TeamID).Attribute['fullName'];
	        	Label19.Caption:=format('Pos %d   |  %d mp (%d-%d-%d-%d)   |  Goals %d-%d (%s)   |  %d pts (%.1f pts/m)', [k+1, M, p3, p2, p1, p0, GF, GA, PlusMinus(GDiff), Pts, Pts/M]);
						Label16.Caption:=format('%d - %d (average: %.2f)', [posRange[k].min, posRange[k].max, posRange[k].avg])
					end;
        Panel1.Visible:=true; break
      end
end;

end.
