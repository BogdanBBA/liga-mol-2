object FAbout: TFAbout
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FAbout'
  ClientHeight = 401
  ClientWidth = 377
  Color = 4209709
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  PixelsPerInch = 96
  TextHeight = 24
  object Panel1: TPanel
    Left = 16
    Top = 16
    Width = 345
    Height = 369
    BevelWidth = 4
    TabOrder = 0
    object Label7: TLabel
      Left = 16
      Top = 10
      Width = 130
      Height = 34
      Caption = 'Liga Mol 2'
      Font.Charset = ANSI_CHARSET
      Font.Color = 2861823
      Font.Height = -27
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 50
      Width = 93
      Height = 17
      Caption = 'by BogdanBBA'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 74
      Width = 93
      Height = 18
      Caption = 'Version history:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu Medium'
      Font.Style = []
      ParentFont = False
    end
    object closeB: TButton
      Left = 88
      Top = 320
      Width = 153
      Height = 34
      Caption = 'Close'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -21
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = closeBClick
    end
    object Memo1: TMemo
      Left = 16
      Top = 98
      Width = 305
      Height = 207
      BorderStyle = bsNone
      Color = 4209709
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 14542812
      Font.Height = -13
      Font.Name = 'Ubuntu Mono'
      Font.Style = []
      Lines.Strings = (
        'v2.0 alpha - September 23rd, 2012'
        'v2.0 beta - September 24th and 25th, 2012'
        'v2.0 - September 26th, 2012'
        'v2.1 - October 29th, 2012'
        'v2.2 - November 4th, 2012'
        'v2.3 beta - November 6th, 2012'
        'v2.3 - January 18th, 2013'
        'v2.4 - January 19th, 2013'
        'v2.5 - September 24th, 2013'
        'v2.51 - October 27th, 2013'
        'v2.6 - December 5th, 2013'
        'v2.7 - January 10th, 2014')
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
    end
  end
end
