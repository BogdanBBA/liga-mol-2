unit TeamEvolution;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, XML.VerySimple, PngImage, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.Series;

type
  TFTeamEvol = class(TForm)
    closeB: TButton;
    ch: TChart;
    Series1: TBarSeries;
    Panel1: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Panel2: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    procedure timgClick(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure chClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTeamEvol: TFTeamEvol;
  timg: array of TImage;

implementation

{$R *.dfm}

uses functii, Mol2pas, DataTypes;

procedure TFTeamEvol.chClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var k, n: integer; team, Oteam: string;
begin
  try
  team:=Label14.Caption;
  if Series = ch.SeriesList[0] then
    begin
      n:=-1; k:=0;
      while n<ValueIndex do
        begin
          if rs[k].Played and ((rs[k].HomeT=team) or (rs[k].AwayT=team)) then inc(n);
          inc(k)
        end;
      inc(k, -1);
      if rs[k].HomeT=team then Oteam:=rs[k].AwayT else Oteam:=rs[k].HomeT;
      Label15.Caption:=format('Mol %s vs. %s', [rs[k].ID, OTeam]);
      Label21.Caption:=formatdatetime('ddd, d mmm yyyy, h:nn', rs[k].When);
      Label20.Caption:=T.Find('team', 'ID', rs[k].HomeT).Attribute['city'];
      Label19.Caption:=format('%s (%s) ', [FormatScore(k), FormatPeriodScores(k)]);
      if NResultPoints(rs[k], team)>1 then Label19.Caption:=Label19.Caption+'(win)'
      else Label19.Caption:=Label19.Caption+'(loss)';
      if rs[k].Attendance.Value<>0 then Label22.Caption:=grupat(rs[k].Attendance.Value)
      else Label22.Caption:='unknown';
      Panel2.Visible:=true
    end
  except on E: Exception do
  	MessageDlg(Format('TFTeamEvol.chClickSeries ERROR:%s%s - %s', [dnl, E.ClassName, E.Message]), mtError, [mbIgnore], 0)
  end
end;

procedure TFTeamEvol.closeBClick(Sender: TObject);
begin
  FTeamEvol.Close
end;

procedure TFTeamEvol.FormResize(Sender: TObject);
begin
  ch.Width:=FTeamEvol.Width-2*ch.Left; ch.Height:=FTeamEvol.Height-ch.Top-ch.Left;
end;

procedure TFTeamEvol.FormCreate(Sender: TObject);
var i: word;
begin
	try
    setlength(timg, nt);
      for i:=0 to nt-1 do
        begin
          timg[i]:=TImage.Create(Self); timg[i].Parent:=FTeamEvol; F1.EnablePNGForImage(timg[i]); timg[i].AutoSize:=true;
          timg[i].Name:='img'+LettersAndDigits(T.ChildNodes[i].Attribute['ID']); timg[i].Hint:=t.ChildNodes[i].Attribute['ID'];
          try timg[i].Picture.LoadFromFile('data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png')
          except MessageDlg('TFTeamEvol.FormCreate: Failed to load team image file "data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png".', mtError, [mbIgnore], 0) end;
          timg[i].Top:=8; timg[i].Left:=closeB.Left+closeB.Width+8+i*timg[0].Width;
          timg[i].Cursor:=crHandPoint; timg[i].OnClick:=timgClick
        end;
      Panel1.Left:=timg[nt-1].Left+timg[nt-1].Width+8;
      Panel2.Left:=Panel1.Left+Panel1.Width+8
  except on E: Exception do
  	MessageDlg(Format('TFTeamEvol.FormCreate ERROR:%s%s - %s', [dnl, E.ClassName, E.Message]), mtError, [mbIgnore], 0)
  end
end;

procedure TFTeamEvol.timgClick(Sender: TObject);
var team: string; i, xi, maxAtt: word; tms, mols: TValStats; aBS: TLineSeries; pBS: TBarSeries;
begin
  try
  team:=TImage(Sender).Hint; Label14.Caption:=team;
  // Get data
  aBS:=TLineSeries.Create(Self); pBS:=TBarSeries.Create(Self);
  aBS.LinePen.Width:=6;
  //
  maxAtt:=0;
  ResetValStats(tms); ResetValStats(mols);
  for i:=0 to nr-1 do if rs[i].Played and (rs[i].Attendance.Value>0) then
    begin
      xi:=rs[i].Attendance.Value;
      inc(mols.count); inc(mols.total, xi);
      if xi>mols.max then mols.max:=xi; if xi<mols.min then mols.min:=xi;
      if rs[i].HomeT=team then
        begin
          inc(tms.count); inc(tms.total, xi);
          if xi>tms.max then tms.max:=xi; if xi<tms.min then tms.min:=xi;
        end
    end;
  if tms.count<>0 then tms.avg:=tms.total/tms.count;
  if mols.count<>0 then mols.avg:=mols.total/mols.count;

  if mols.avg=0 then
  	begin
    	MessageDlg('Please update the attendance data before trying to view the stats.'+dnl+'In a future release, the previous attendance data will be retained during result updates.', mtWarning, [mbOK], 0);
			closeBClick(FTeamEvol);
      Exit
    end;
  //
  for i:=0 to nr-1 do if rs[i].Played and (rs[i].HomeT=team) then
      if rs[i].Attendance.Value>maxAtt then maxAtt:=rs[i].Attendance.Value;
  maxAtt:=round(1.1*maxAtt);
  for i:=0 to nr-1 do
    if rs[i].Played then
      begin
        if rs[i].HomeT=team then
          begin
            pBS.AddBar(round(maxAtt*(NResultPoints(rs[i], team)+1)/4), format('%s:%d', [rs[i].AwayT, NResultPoints(rs[i], team)]), $008C6037);
            if rs[i].Attendance.Value<>0 then
              aBS.AddXY(pBS.Count-1, rs[i].Attendance.Value, formatdatetime('d/mmm/yyyy', rs[i].When), $0021B467)
          end;
        if rs[i].AwayT=team then
          begin
            pBS.AddBar(round(maxAtt*(NResultPoints(rs[i], team)+1)/4), format('%s:%d', [rs[i].HomeT, NResultPoints(rs[i], team)]), $00350DAC)
          end;
      end;
  // Stats
  Label3.Caption:=grupat(mols.min)+'-'+grupat(mols.max);
  Label5.Caption:=format('%s (%s)', [grupat(round(mols.avg)), plural('match', mols.count, true)]);
  Label7.Caption:=grupat(mols.total);
  Label13.Caption:=grupat(tms.min)+'-'+grupat(tms.max);
  Label10.Caption:=format('%s (%s)', [grupat(round(tms.avg)), stringreplace(plural('match', tms.count, true), 'match', 'home match', [rfReplaceAll])]);
  Label9.Caption:=format('%s (%.2f%%)', [grupat(tms.total), tms.total/mols.total*100]);
  // Format chart
  ch.ClearChart; ch.View3D:=false; ch.Legend.Visible:=false;
  ch.AddSeries(pBS); ch.AddSeries(aBS);
  Panel2.Visible:=false
  except on E: Exception do
  	MessageDlg(Format('TFTeamEvol.timgClick ERROR:%s%s - %s', [dnl, E.ClassName, E.Message]), mtError, [mbIgnore], 0)
  end
end;

end.
