unit Team;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, XML.VerySimple, PngImage;

type MFStatus=(mfPlayed, mfUpcoming);
     MFVenue=(mfHome, mfAway);
     MFResult=(mf3p, mf2p, mf1p, mf0p);

     MFStatusSet=set of MFStatus;
     MFVenueSet=set of MFVenue;
     MFResultSet=set of MFResult;

type
  TFTeam = class(TForm)
    bigPanel: TPanel;
    closeB: TButton;
    Panel17: TPanel;
    Label11: TLabel;
    l3: TLabel;
    l4: TLabel;
    l5: TLabel;
    l6: TLabel;
    l7: TLabel;
    l8: TLabel;
    l9: TLabel;
    l10: TLabel;
    frS8: TLabel;
    frS7: TLabel;
    frS6: TLabel;
    frS5: TLabel;
    frS4: TLabel;
    frS3: TLabel;
    frS2: TLabel;
    frS1: TLabel;
    Label12: TLabel;
    frS9: TLabel;
    SB: TScrollBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    ll1: TLabel;
    ll2: TLabel;
    ll3: TLabel;
    ll4: TLabel;
    ll6: TLabel;
    ll5: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Panel3: TPanel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Image2: TImage;
    Image3: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label13: TLabel;
    pSt1: TPanel;
    pSt2: TPanel;
    pSt3: TPanel;
    pV3: TPanel;
    pV2: TPanel;
    pV1: TPanel;
    pR1: TPanel;
    pR2: TPanel;
    pR4: TPanel;
    pR5: TPanel;
    pR6: TPanel;
    pR3: TPanel;
    pR7: TPanel;
    tCB: TComboBox;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure initiateRefresh;
    procedure filterAndRefreshResults(fSt: MFStatusSet; fV: MFVenueSet; fR: MFResultSet; agTeam: word);
    procedure timgClick(Sender: TObject);
    procedure GenericMouseEnter(Sender: TObject);
    procedure GenericMouseLeave(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pSt1Click(Sender: TObject);
    procedure pV1Click(Sender: TObject);
    procedure pR1Click(Sender: TObject);
    procedure llClick(Sender: TObject);
    procedure tCBChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TResultLabelLine=record
  IDL, WhenL, HomeTL, AwayTL, ResultL, PeriodsL: TLabel;
end;

var
  FTeam: TFTeam;
  timg: array of TImage;
  ll: array of TResultLabelLine;
  FTeamToShowX, lastTLTop: integer;

implementation

{$R *.dfm}

uses DataTypes, functii, Mol2pas;

procedure TFTeam.closeBClick(Sender: TObject);
begin
  Fteam.Close
end;

procedure TFTeam.GenericMouseEnter(Sender: TObject);
begin if Sender is TPanel then TPanel(Sender).Font.Color:=clYellow else if Sender is TLabel then TLabel(Sender).Font.Color:=clYellow end;

procedure TFTeam.GenericMouseLeave(Sender: TObject);
begin if Sender is TPanel then TPanel(Sender).Font.Color:=clWhite else if Sender is TLabel then TLabel(Sender).Font.Color:=clWhite end;

procedure TFTeam.FormCreate(Sender: TObject);
  {*}procedure CopyLabelFormatting(sL: TLabel; var dL: TLabel);
  begin
    if sl=nil then MessageDlg('TFTeam.FormCreate() ERROR: source label = nil', mtError, [mbIgnore], 0);
    if dl=nil then begin dl:=TLabel.Create(Self); dl.Parent:=FTeam.SB end;
    dl.AutoSize:=sl.AutoSize; dl.Left:=sl.Left; dl.Width:=sl.Width; dl.Height:=sl.Height; dl.Top:=lastTLTop;
    dl.Font:=sl.Font; dl.Alignment:=sl.Alignment; dl.Visible:=sl.Visible;
    dl.OnMouseEnter:=sl.OnMouseEnter; dl.OnMouseLeave:=sl.OnMouseLeave
  end;
var i: word;
begin
  setlength(timg, nt);
  for i:=0 to nt-1 do
    begin
      timg[i]:=TImage.Create(Self); timg[i].Parent:=Panel2; F1.EnablePNGForImage(timg[i]); timg[i].AutoSize:=true;
      timg[i].Name:='img'+LettersAndDigits(T.ChildNodes[i].Attribute['ID']); timg[i].Hint:=t.ChildNodes[i].Attribute['ID'];
      try timg[i].Picture.LoadFromFile('data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png') except MessageDlg('TFTeam.FormCreate: Failed to load team image file "data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png".', mtError, [mbIgnore], 0) end;
      timg[i].Top:=0; timg[i].Left:=i*timg[0].Width;
      timg[i].Cursor:=crHandPoint; timg[i].OnClick:=timgClick
    end;
  Panel2.Width:=nt*timg[0].Width; Panel2.Left:=Panel1.Width div 2 - Panel2.Width div 2;
  //
  setlength(ll, nr); lastTLTop:=64;
  for i:=0 to length(ll)-1 do
    begin
      CopyLabelFormatting(ll1, ll[i].IDL);
      CopyLabelFormatting(ll2, ll[i].WhenL);
      CopyLabelFormatting(ll3, ll[i].HomeTL); ll[i].HomeTL.OnClick:=llClick;
      CopyLabelFormatting(ll4, ll[i].ResultL);
      CopyLabelFormatting(ll5, ll[i].PeriodsL);
      CopyLabelFormatting(ll6, ll[i].AwayTL); ll[i].AwayTL.OnClick:=llClick;
      inc(lastTLTop, 22)
    end;
end;

procedure TFTeam.FormShow(Sender: TObject);
var x, i: integer;
begin
  FTeam.ShowHint:=F1.ShowHint;
  tCB.Clear; tCB.Items.Add('ANY TEAM'); for i:=0 to nt-1 do tCB.Items.Add(T.ChildNodes[i].Attribute['ID']); tCB.ItemIndex:=0;
  if FTeamToShowX<>-1 then begin timgClick(timg[FTeamToShowX]); FTeamToShowX:=-1; exit end;
  x:=-1; for i:=0 to nt-1 do if T.ChildNodes[i].Attribute['ID']=favT then begin x:=i; break end;
  if x<>-1 then timgClick(timg[x])
end;

procedure TFTeam.pSt1Click(Sender: TObject);
var i: word;
begin
  for i:=1 to 3 do if TPanel(FindComponent('pSt'+inttostr(i)))=Sender then
    TPanel(FindComponent('pSt'+inttostr(i))).Font.Style:=[fsBold]
  else
    TPanel(FindComponent('pSt'+inttostr(i))).Font.Style:=[];
  initiateRefresh
end;

procedure TFTeam.pV1Click(Sender: TObject);
var i: word;
begin
  for i:=1 to 3 do if TPanel(FindComponent('pV'+inttostr(i)))=Sender then
    TPanel(FindComponent('pV'+inttostr(i))).Font.Style:=[fsBold]
  else
    TPanel(FindComponent('pV'+inttostr(i))).Font.Style:=[];
  initiateRefresh
end;

procedure TFTeam.pR1Click(Sender: TObject);
var i: word;
begin
  for i:=1 to 7 do
    if TPanel(FindComponent('pR'+inttostr(i)))=Sender then TPanel(FindComponent('pR'+inttostr(i))).Font.Style:=[fsBold]
    else TPanel(FindComponent('pR'+inttostr(i))).Font.Style:=[];
  initiateRefresh
end;

procedure TFTeam.llClick(Sender: TObject);
begin timgClick(FindComponent('img'+LettersAndDigits(TLabel(Sender).Caption))) end;

procedure TFTeam.tCBChange(Sender: TObject);
begin
  initiateRefresh //leave tCBChange separate
end;

procedure TFTeam.timgClick(Sender: TObject);
var xs: string;
begin
  with T.Find('team', 'ID', TImage(Sender).Hint) do
    begin
      xs:='data\img\teams\large\'+Attribute['ID']+'.png';
      try Image1.Picture.LoadFromFile(xs) except MessageDlg('TFTeam.timgClick: Failed to load team image file "'+xs+'".', mtError, [mbIgnore], 0) end;
      xs:='data\img\cities\'+copy(Attribute['city'], 1, pos(',', Attribute['city'])-1)+'.png';
      try Image2.Picture.LoadFromFile(xs) except MessageDlg('TFTeam.timgClick: Failed to load team image file "'+xs+'".', mtError, [mbIgnore], 0) end;
      xs:='data\img\countries\'+copy(Attribute['city'], pos(',', Attribute['city'])+2, 3)+'.png';
      try Image3.Picture.LoadFromFile(xs) except MessageDlg('TFTeam.timgClick: Failed to load team image file "'+xs+'".', mtError, [mbIgnore], 0) end;
      Label1.Caption:=Attribute['fullName']; Label2.Caption:=Attribute['ID'];
    end;
  initiateRefresh
end;

procedure TFTeam.initiateRefresh;
  {*}function GetSelectedPanelSuffix(gen: string; fromI, toI: word): integer;
  var i: word;
  begin
    result:=-1; for i:=fromI to toI do
      if TPanel(FindComponent(gen+inttostr(i))).Font.Style=[fsBold] then
        begin result:=i; break end
  end;
var st: MFStatusSet; v: MFVenueSet; r: MFResultSet; agT: word; allResults: boolean;
begin
  allResults:=false;
  case GetSelectedPanelSuffix('pSt', 1, 3) of
  1: st:=[mfPlayed, mfUpcoming];
  2: st:=[mfPlayed];
  3: st:=[mfUpcoming]
  else st:=[] end;
  case GetSelectedPanelSuffix('pV', 1, 3) of
  1: v:=[mfHome, mfAway];
  2: v:=[mfHome];
  3: v:=[mfAway]
  else st:=[] end;
  case GetSelectedPanelSuffix('pR', 1, 7) of
  1: begin r:=[mf3p, mf2p, mf1p, mf0p]; allResults:=true end;
  2: r:=[mf3p, mf2p];
  3: r:=[mf1p, mf0p];
  4: r:=[mf3p];
  5: r:=[mf2p];
  6: r:=[mf1p];
  7: r:=[mf0p]
  else st:=[] end;
  agT:=tCB.ItemIndex;
  if not allResults then st:=st-[mfUpcoming];
  //
  filterAndRefreshResults(st, v, r, agT)
end;

procedure TFTeam.filterAndRefreshResults(fSt: MFStatusSet; fV: MFVenueSet; fR: MFResultSet; agTeam: word);
  {*}procedure SetVisibilityForResultLine(x: word; viz: boolean);
  begin
    with ll[x] do
      begin IDL.Visible:=viz; WhenL.Visible:=viz; HomeTL.Visible:=viz; AwayTL.Visible:=viz; ResultL.Visible:=viz; PeriodsL.Visible:=viz end
  end;
var S, xs: string; ml, fl: TStringList; k1, k2, k3, k4: boolean; p, i, j, teamG, advG: integer; frS: TStandingsPosition;
begin
  // Initialize
  S:=Label2.Caption;
  ml:=TStringList.Create; ml.Sorted:=true; ml.Duplicates:=dupIgnore;
  fl:=TStringList.Create; fl.Sorted:=true; fl.Duplicates:=dupIgnore;
  // Get complete list of matches involving team S
  for i:=0 to nr-1 do if (ansilowercase(S)=ansilowercase(rs[i].HomeT)) or (ansilowercase(S)=ansilowercase(rs[i].AwayT)) then ml.Add(rs[i].ID);
  // If all the criteria of each match in the list are fulfilled, then add the match to the filtered list
  if ml.Count>0 then for i:=0 to ml.Count-1 do
    begin
      p:=ResultPosByID(ml[i]);
      if rs[p].Played then k1:=mfPlayed in fSt else k1:=mfUpcoming in fSt;
      if S=rs[p].HomeT then k2:=mfHome in fV else k2:=mfAway in fV;
      case NResultPoints(rs[p], S) of 3: k3:=mf3p in fR; 2: k3:=mf2p in fR; 1: k3:=mf1p in fR; 0: k3:=mf0p in fR else k3:=false end;
      if agTeam=0 then k4:=true else k4:=(T.ChildNodes[agTeam-1].Attribute['ID']=rs[p].HomeT) or (T.ChildNodes[agTeam-1].Attribute['ID']=rs[p].AwayT);
      if k1 and k2 and k3 and k4 then fl.Add(ml[i])
    end;
  // Calculate the stats
    //sort by date&time
    if fl.Count>1 then for i:=0 to fl.Count-2 do for j:=i+1 to fl.Count-1 do
      if rs[ResultPosByID(fl[i])].When>rs[ResultPosByID(fl[j])].When then fl.Exchange(i, j);
    //reset and add to frS
    with frS do begin TeamID:=S; M:=0; p3:=0; p2:=0; p1:=0; p0:=0; GF:=0; GA:=0; GDiff:=0; Pts:=0 end;
    if fl.Count>0 then for i:=0 to fl.Count-1 do
      with rs[ResultPosByID(fl[i])] do
        begin
          if HomeT=S then begin teamG:=ResultHG; advG:=ResultAG end else begin teamG:=ResultAG; advG:=ResultHG end;
          case NResultPoints(rs[ResultPosByID(fl[i])], S) of 3: inc(frS.p3); 2: inc(frS.p2); 1: inc(frS.p1); 0: inc(frS.p0); end;
//          if nPeriods=3 then begin inc(frS.p3, integer(teamG>advG)); inc(frS.p0, integer(teamG<advG)) end
//          else begin inc(frS.p2, integer(teamG>advG)); inc(frS.p1, integer(teamG<advG)) end;
          with frS do begin M:=p3+p2+p1+p0; inc(GF, teamG); inc(GA, advG); GDiff:=GF-GA; Pts:=3*p3+2*p2+p1 end
        end;
  // Show the filtered results
  for i:=0 to length(ll)-1 do
    SetVisibilityForResultLine(i, i<fl.Count);
  for i:=0 to fl.Count-1 do
    begin
      p:=ResultPosByID(fl[i]);
      ll[i].IDL.Caption:=fl[i];
      ll[i].WhenL.Caption:=formatdatetime('ddd, d mmmm yyyy, h:nn', rs[p].When);
      ll[i].HomeTL.Caption:=rs[p].HomeT;
      ll[i].AwayTL.Caption:=rs[p].AwayT;
      ll[i].ResultL.Caption:=FormatScore(p);
      ll[i].PeriodsL.Caption:=FormatPeriodScores(p)
    end;
  // Show the stats
  Label7.Caption:='Out of the '+Plural('match', ml.Count, true)+' involving team '+S+', '+Plural('has', fl.Count, true)+' been filtered.';
  frS1.Caption:=inttostr(frS.M); frS2.Caption:=inttostr(frS.p3); frS3.Caption:=inttostr(frS.p2); frS4.Caption:=inttostr(frS.p1);
  frS5.Caption:=inttostr(frS.p0); frS6.Caption:=format('%d-%d', [frS.GF, frS.GA]); frS7.Caption:=PlusMinus(frS.GDiff); frS8.Caption:=inttostr(frS.Pts);
  if frS.M<=0 then frS9.Caption:='-'
  else begin xs:=floattostr(frS.Pts/frS.M); if length(xs)>4 then xs:=copy(xs, 1, 4); frS9.Caption:=xs end;
  //
  ml.Free; fl.Free
end;

end.
