﻿unit Settings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFSett = class(TForm)
    Label7: TLabel;
    closeB: TButton;
    saveB: TButton;
    Label1: TLabel;
    cb1: TComboBox;
    Label2: TLabel;
    ch1: TCheckBox;
    closeT: TTimer;
    Label3: TLabel;
    ch2: TCheckBox;
    procedure closeBClick(Sender: TObject);
    procedure saveBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure closeTTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSett: TFSett;

implementation

{$R *.dfm}

uses DataTypes, functii, Mol2pas, Team, XML.VerySimple;

procedure TFSett.closeBClick(Sender: TObject);
begin
  FSett.Close
end;

procedure TFSett.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	F1.RefreshStandings
end;

procedure TFSett.FormShow(Sender: TObject);
var i: word;
begin
  with saveB do begin Enabled:=true; Caption:='Save' end;
  cb1.Clear; for i:=0 to nt-1 do cb1.Items.Add(T.ChildNodes[i].Attribute['ID']); cb1.ItemIndex:=cb1.Items.IndexOf(favT);
  ch1.Checked:=TrueFalseToBoolean(Sett.Find('ShowHints').Attribute['value'], true);
  ch2.Checked:=TrueFalseToBoolean(Sett.Find('ShowFullTeamNameInStandings').Attribute['value'], true)
end;

procedure TFSett.saveBClick(Sender: TObject);
begin
  try
  if cb1.ItemIndex=-1 then favT:=T.ChildNodes[0].Attribute['ID'] else favT:=cb1.Text;
  //
  Sett.Find('ShowHints').SetAttribute('value', booleantotruefalse(ch1.Checked));
  Sett.Find('ShowFullTeamNameInStandings').SetAttribute('value', booleantotruefalse(ch2.Checked));
  // also apply where necessary
  F1.ShowHint:=ch1.Checked; FTeam.ShowHint:=ch1.Checked;
  //
  Sett.Find('ShowHints').SetAttribute('value', Sett.Find('ShowHints').Attribute['value']);
  Sett.Find('ShowFullTeamNameInStandings').SetAttribute('value', Sett.Find('ShowFullTeamNameInStandings').Attribute['value']);
  Sett.Find('FavoriteTeam').SetAttribute('value', favT);
  if not CopyFile(PWideChar('data\settings.xml'), PWideChar('data\backups\settings '+formatdatetime('yyyy.mm.dd hh.nn', now)+'.xml'), false) then
    if MessageDlg('The file "data\settings.xml" could not be backed up.'+dnl+'Would you like to continue?', mtWarning, [mbYes, mbCancel], 0)=mrCancel then Exit;
  // until i figure out how to delete nodes properly, i'll do it the hard way
  with xSett.Root.Find('Settings') do
    begin
      Find('FavoriteTeam').SetAttribute('value', favT);
      Find('ShowHints').SetAttribute('value', Sett.Find('ShowHints').Attribute['value']);
      Find('ShowFullTeamNameInStandings').SetAttribute('value', Sett.Find('ShowFullTeamNameInStandings').Attribute['value']);
      Find('CoordForImageMaxWidth').SetAttribute('value', Sett.Find('CoordForImageMaxWidth').Attribute['value'])
    end;
  xSett.SaveToFile('data\settings.xml');
  with saveB do begin Enabled:=false; Caption:='Saved!' end; closeT.Enabled:=true
  except on E:Exception do begin MessageDlg('ERROR in TFSett.saveBClick'+dnl+E.ClassName+': '+E.message, mtError, [mbOK], 0); exit end
  end
end;

procedure TFSett.closeTTimer(Sender: TObject);
begin
  closeT.Enabled:=false; closeBClick(closeT)
end;

end.
