unit AttendanceUpdater;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  ShellAPI, FastHtmlParser, XML.VerySimple, DataTypes, UIntList;

type
  TFAttUpd = class(TForm)
    Panel1: TPanel;
    Label7: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    closeB: TButton;
    Label2: TLabel;
    stat2: TLabel;
    stat1: TLabel;
    downloadT: TTimer;
    procedure FormShow(Sender: TObject);
    procedure downloadTTimer(Sender: TObject);
    procedure FinalizeAndSave;
    procedure closeBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAttUpd: TFAttUpd;
  sl, errL: TStringList;
  ir: integer;

implementation

{$R *.dfm}

uses functii, Mol2pas;

procedure TFAttUpd.FormShow(Sender: TObject);
begin
  // Backing up
  stat1.Caption:='Initializing update...'; stat2.Caption:='Backing up data...';
  if not CopyFile(PWideChar(getcurrentdir+'\data\data.xml'), PWideChar(getcurrentdir+'\data\backups\data '+formatdatetime('yyyy.mm.dd hh.nn', now)+'.xml'), false) then
      if MessageDlg('The file "data\data.xml" could not be backed up.'+dnl+'Would you like to continue?', mtWarning, [mbYes, mbCancel], 0)=mrCancel then
        begin {timer?} Exit end;
  // Starting downloads
  sl:=TStringList.Create; sl.Sorted:=false; sl.Duplicates:=dupAccept;
  errL:=TStringList.Create; errL.Sorted:=false; errL.Duplicates:=dupAccept;
  ir:=-1; downloadT.Enabled:=true
end;

procedure TFAttUpd.downloadTTimer(Sender: TObject);
var dldOK: boolean; xs: string;
begin
  downloadT.Enabled:=false; inc(ir);
  if ir<nr then
    begin
      stat1.Caption:=format('Downloading data for match %d/%d (%.2f%%)', [ir+1, nr, (ir+1)/nr*100]);
      // Downloading HTML
      if rs[ir].Attendance.HTMLMatchAddress<>'' then
        begin
          stat2.Caption:=format('Webpage "%s"...', [rs[ir].Attendance.HTMLMatchAddress]);
          dldOK := GetInetFile(rs[ir].Attendance.HTMLMatchAddress, getcurrentdir+'\data\temp.html')
        end
      else dldOK := false;
      // Decode if downloaded
      if dldOK then
        begin
          sl.LoadFromFile(getcurrentdir+'\data\temp.html'); xs:=sl.Text;
          try
            delete(xs, 1, pos('Spectators', xs));
            delete(xs, 1, pos('"c28">', xs)+length('"c28">')-1);
            delete(xs, pos('<', xs), length(xs));
            rs[ir].Attendance.Value := strtoint(xs)
          except
            rs[ir].Attendance.Value := 0;
            errL.Add(rs[ir].ID)
          end;
        end;
      downloadT.Enabled:=true
    end
  else FinalizeAndSave
end;

procedure TFAttUpd.FinalizeAndSave;
var {fn: string;} i: word; xs: string; //ax: TXMLVerySimple;
begin
  try
    // Save
    stat2.Caption:='Saving updated data ...';
    for i:=0 to nr-1 do
      with x.Root.Find('Results').Find('result', 'ID', rs[i].ID) do
        begin
          if not HasAttribute('attendance') then SetAttribute('attendance', '0');
          Attribute['attendance'] := inttostr(rs[i].Attendance.Value)
        end;
    x.SaveToFile(getcurrentdir+'\data\data.xml');
    // Export
    {stat2.Caption:='Exporting attendance data ...';
    fn:='data\exports\attendances '+formatdatetime('yyyy-mm-dd', now)+'.xml';
    ax:=TXMLVerySimple.Create; ax.Root.NodeName:='MolLiga2_Attendances'; ax.Root.SetAttribute('modified', formatdatetime('yyyy/mmm/dd hh:nn:ss', now));
    for i:=0 to nr-1 do
      begin
        ax.Root.AddChild('game');
        with ax.Root.ChildNodes.Last do
          begin
            SetAttribute('played', booleantotruefalse(rs[i].Played));
            SetAttribute('when', formatdatetime('yyyy/mm/dd hh:nn', rs[i].When));
            SetAttribute('attendance', inttostr(rs[i].Attendance.Value));
            SetAttribute('result', format('%d-%d', [rs[i].ResultHG, rs[i].ResultAG]));
            SetAttribute('homeT', rs[i].HomeT);
            SetAttribute('awayT', rs[i].AwayT)
          end;
      end;
    ax.SaveToFile(fn); ax.Free;}
    if errL.Count=0 then stat2.Caption:='Everything has completed successfully!'
    else stat2.Caption:='Updating has completed with '+plural('error', errL.Count, true)+'.';
    if errL.Count<>0 then
      begin
        xs:='Some download/conversion errors have occured. The data has not been affected by this, as the attendance values have simply been set to 0.'+dnl+'The list of match IDs with errors: ';
        for i:=0 to errL.Count-2 do xs:=xs+errL[i]+', '; xs:=xs+errL[errL.Count-1]+'.';
        MessageDlg(xs, mtWarning, [mbIgnore], 0)
      end;
    //ShellExecute(Handle, 'open', PChar(fn), nil, nil, SW_SHOW)
  except on E:Exception do
    MessageDlg(format('%s%s%s - %s', ['An error has occured in TFAttUpd.FinalizeAndSave. See if the error message below points towards a solution.', dnl, E.ClassName, E.Message]), mtError, [mbCancel], 0)
  end;
end;

procedure TFAttUpd.closeBClick(Sender: TObject);
begin
  downloadT.Enabled:=false;
  sl.Free; errL.Free;
  FAttUpd.Close
end;

end.
