object FStandsingsEvol: TFStandsingsEvol
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FStandsingsEvol'
  ClientHeight = 428
  ClientWidth = 940
  Color = 4209709
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 624
    Top = 104
    Width = 63
    Height = 26
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -20
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label2: TLabel
    Left = 624
    Top = 136
    Width = 28
    Height = 16
    Caption = 'Label2'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Ubuntu Condensed'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label3: TLabel
    Left = 624
    Top = 158
    Width = 41
    Height = 17
    Caption = 'Label3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label4: TLabel
    Left = 200
    Top = 8
    Width = 46
    Height = 19
    Caption = 'Label4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Shape1: TShape
    Left = 624
    Top = 200
    Width = 65
    Height = 65
    Brush.Color = 8014636
    Visible = False
  end
  object Label5: TLabel
    Left = 408
    Top = 14
    Width = 82
    Height = 19
    Caption = 'Vertical grid'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 408
    Top = 38
    Width = 102
    Height = 19
    Caption = 'Horizontal grid'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object closeB: TButton
    Left = 8
    Top = 8
    Width = 177
    Height = 74
    Caption = 'Close'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = closeBClick
  end
  object ch: TChart
    Left = 8
    Top = 88
    Width = 561
    Height = 258
    Title.Text.Strings = (
      'TChart')
    OnClickSeries = chClickSeries
    LeftAxis.ExactDateTime = False
    LeftAxis.Increment = 7.000000000000000000
    LeftAxis.Inverted = True
    View3D = False
    View3DWalls = False
    TabOrder = 1
    PrintMargins = (
      15
      27
      15
      27)
    ColorPaletteIndex = 13
    object Series1: TBarSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Bar'
      YValues.Order = loNone
    end
  end
  object tb1: TTrackBar
    Left = 191
    Top = 32
    Width = 178
    Height = 33
    Min = 1
    Position = 7
    TabOrder = 2
    OnChange = tb1Change
  end
  object ch1: TCheckBox
    Left = 392
    Top = 16
    Width = 99
    Height = 17
    TabOrder = 3
    OnClick = ch1Click
  end
  object ch2: TCheckBox
    Left = 392
    Top = 40
    Width = 122
    Height = 17
    Checked = True
    State = cbChecked
    TabOrder = 4
    OnClick = ch1Click
  end
  object Panel1: TPanel
    Left = 510
    Top = 0
    Width = 430
    Height = 82
    BevelOuter = bvNone
    TabOrder = 5
    object Label16: TLabel
      Left = 104
      Top = 49
      Width = 41
      Height = 17
      Caption = 'Range:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 8
      Top = 49
      Width = 90
      Height = 17
      Caption = 'Position range:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label18: TLabel
      Left = 8
      Top = 34
      Width = 62
      Height = 17
      Caption = 'Standings:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel
      Left = 104
      Top = 34
      Width = 41
      Height = 17
      Caption = 'Range:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label20: TLabel
      Left = 8
      Top = 2
      Width = 154
      Height = 36
      CustomHint = F1.BalloonHint1
      Caption = 'MOL League'
      Font.Charset = ANSI_CHARSET
      Font.Color = 649392
      Font.Height = -29
      Font.Name = 'Signika'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
end
