﻿unit Updater;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, WinINet, Vcl.ExtCtrls, Vcl.StdCtrls,
  ShellAPI, FastHtmlParser, XML.VerySimple, DataTypes;

const clWaiting=$004080FF;
      clSuccess=$0000FF80;
      clError=clRed;

type
  TFUpdate = class(TForm)
    Panel1: TPanel;
    downloadT: TTimer;
    Label7: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    sh1: TShape;
    Label2: TLabel;
    sh2: TShape;
    Label3: TLabel;
    sh3: TShape;
    Label5: TLabel;
    closeB: TButton;
    timeoutT: TTimer;
    statusL: TLabel;
    extractT: TTimer;
    finishT: TTimer;
    procedure FormShow(Sender: TObject);
    procedure downloadTTimer(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure timeoutTTimer(Sender: TObject);
    procedure extractTTimer(Sender: TObject);
    procedure finishTTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FUpdate: TFUpdate;
  z: TXMLVerySimple;
  zl: TXMLNodeList;
  ur: array of TResult;
  nur: word;

implementation

{$R *.dfm}

uses functii, Mol2pas;

procedure TFUpdate.closeBClick(Sender: TObject);
begin FUpdate.Close end;

procedure TFUpdate.timeoutTTimer(Sender: TObject);
begin
  if MessageDlg('The download has been going on for quite some time now.'+dnl+'Would you like to halt the process?', mtWarning, mbYesNo, 0)=mrYes then Halt
end;

procedure TFUpdate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //
end;

procedure TFUpdate.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (sh1.Brush.Color=clSuccess) and (sh2.Brush.Color=clSuccess) and (sh3.Brush.Color=clSuccess) then
    if MessageDlg('The data has been updated successfully!'+dnl+'Would it be OK if I restart the application?', mtConfirmation, mbYesNo, 0)=mrYes then
      begin ShellExecute(Handle, 'open', PChar(getcurrentdir+'\Mol2.exe'), nil, nil, SW_SHOW); F1.exitBClick(FUpdate) end;
  CanClose:=true
end;

procedure TFUpdate.FormShow(Sender: TObject);
var i: word;
begin
  for i:=1 to 3 do TShape(FindComponent('sh'+inttostr(i))).Brush.Color:=clWaiting;
  z:=TXMLVerySimple.Create;
  //
  statusL.Caption:=' Downloading web page... '; downloadT.Enabled:=true
end;

procedure TFUpdate.downloadTTimer(Sender: TObject);
begin
  downloadT.Enabled:=false; timeoutT.Enabled:=true;
  if GetInetFile('http://icehockey.hu/oldalak/mol_liga/menetrend', getcurrentdir+'\data\temp.html') then
    begin sh1.Brush.Color:=clSuccess; statusL.Caption:=' Downloaded web page! Extracting... '; extractT.Enabled:=true end
  else
    begin sh1.Brush.Color:=clError; statusL.Caption:=' An error occured while downloading web page ' end
end;

procedure TFUpdate.extractTTimer(Sender: TObject);
const delTxt: array[0..6] of string=(' _base_target="center"', '<strong>', '</strong>', '<font color="#ffffff">', '</font>', '                </tr>', ' align="center"');
      nChrSet=20;
      chrOP: array[0..nChrSet-1] of char=('á', 'é', 'õ', 'ő', 'ó', 'í', 'Á', 'É', 'Õ', 'Ó', 'Í', 'Ö', 'Ú', 'ö', 'ú', 'ă', 'î', 'â', 'ș', 'ț');
      chrNP: array[0..nChrSet-1] of char=('a', 'e', 'o', 'o', 'o', 'i', 'A', 'E', 'O', 'O', 'I', 'O', 'U', 'o', 'u', 'a', 'i', 'a', 's', 't');
      nChrsSet=8;
      chrsOP: array[0..nChrsSet-1] of string=('Ã©', 'Ã­', 'Ãš', 'Ã¡', 'Ă©', 'Ăˇ', 'Ă­', 'Ăł');
      chrsNP: array[0..nChrsSet-1] of string=('e',  'i',  'U',  'a',  'e',  'a',  'i',  'u');
      nClosedTags=2;
  {*}function GetOnlyNumber(s: string): word;
  var i: word; r: string;
  begin r:=''; for i:=1 to length(s) do if charinset(s[i], ['0'..'9']) then r:=r+s[i]; if r<>'' then result:=strtoint(r) else result:=0 end;
  {*}function GetDateTimeFromHTMLText(d, t: string): TDateTime;
  begin
    if pos(' ', d)<>0 then d:=copy(d, 1, pos(' ', d)-1); if length(d)<>10 then d:='1900/01/01'; if length(t)<>5 then t:='00:00';
    result := GetDate(d+' '+t)
  end;
  {*}function IdentifyTeam(s: string): string; //this is based on the facts that the user-introduced ID is included in the name on the webpage (as these are more complete) AND that they are not mixed (eg. the ID is Kol, and two teams are Kolona and Kolakis)
  var r, i: integer; xs: string;
  begin
    r:=-1; for i:=0 to nt-1 do
    if (pos(ansiuppercase(T.ChildNodes[i].Attribute['ID']), ansiuppercase(s))<>0) or (pos(ansiuppercase(T.ChildNodes[i].Attribute['WPName']), ansiuppercase(s))<>0) then
      begin r:=i; break end;
    if r<>-1 then result:=T.ChildNodes[r].Attribute['ID']
    else begin result:='Error'; xs:=nl; for i:=0 to nt-1 do xs:=xs+nl+T.ChildNodes[i].Attribute['ID']; MessageDlg('IdentifyTeam() WARNING: Could not identify team "'+s+'" in the list below... returning "Erorr", which may produce... errors.'+xs, mtError, [mbIgnore], 0) end
  end;
  {*}function GetMatchSheetHTMLAddress(s: string): string;
  var c: char;
  begin
    c:='1''3'[2];
    if pos(c, s)<>0 then delete(s, 1, pos(c, s));
    if pos(c, s)<>0 then delete(s, pos(c, s), length(s)-pos(c, s)+1);
    result := stringreplace(s, 'merkozes', 'jkmol', [rfReplaceAll])
  end;
  {*}procedure GetResultFromHTML(s: string; var r: TResult);
  begin
  	// Test first if force-result
    if pos('Versenybirusag', s)<>0 then
    	begin
				r.Played:=true; s:=copy(s, 1, pos(' ',s)-1); DecodeResult('0-0;0-0;0-0', r);
        r.ForceResult.Force:=true; r.ForceResult.HomeG:=StrToInt(Copy(s, 1, pos(':',s)-1)); r.ForceResult.AwayG:=StrToInt(Copy(s, pos(':',s)+1, length(s)));
        Exit
      end;
    r.ForceResult.Force:=false;
    if (pos('(', s)<>0) and (pos(')', s)<>0) then
      begin r.Played:=true; s:=copy(s, pos('(',s)+1, pos(')',s)-pos('(',s)-1); s:=MultiStringReplace(s, [':',', '], ['-',';'], [rfReplaceAll]); DecodeResult(s, r) end
    else begin r.Played:=false; r.nPeriods:=3; DecodeResult('0-0;0-0;0-0', r) end
  end;
var sl: TStringList; phz, phz2, xs: string; i, p: longint;
begin
  extractT.Enabled:=false;
  try
    phz:='Trimming file in TStringList...'; phz2:='-';
    sl:=TStringList.Create; sl.LoadFromFile(getcurrentdir+'\data\temp.html');   //showmessagefmt('before: sl.count=%d', [sl.Count]);
    while pos('SORSZ', sl[3])=0 do sl.Delete(0);                                //showmessagefmt('after: sl.count=%d', [sl.Count]);
    p:=4; while pos('</table>', sl[p])=0 do inc(p); while sl.Count>p+1 do sl.Delete(p+1);
    phz:='Replacing weird characters in TStringList...';
    for i:=0 to sl.Count-1 do
      begin xs:=sl[i]; for p:=0 to nChrSet-1 do while pos(chrOP[p], xs)<>0 do xs[pos(chrOP[p], xs)]:=chrNP[p]; sl[i]:=xs end;
    for i:=0 to sl.Count-1 do
      begin xs:=sl[i]; xs:=MultiStringReplace(xs, chrsOP, chrsNP, [rfReplaceAll]); sl[i]:=xs end;
    phz:='Removing weird attributes and tags in TStringList...';
    for p:=low(delTxt) to high(delTxt) do for i:=0 to sl.Count-1 do
      if pos(delTxt[p], sl[i])<>0 then begin xs:=sl[i]; delete(xs, pos(delTxt[p], sl[i]), length(delTxt[p])); sl[i]:=xs end;
    //sl.Insert(0, '<tempFile modified="'+formatdatetime('yyyy/mmm/dd hh:nn:ss', now)+'" />');
    sl.Insert(0, '<?xml version="1.0" encoding="utf-8" ?>');
    sl.SaveToFile(getcurrentdir+'\data\temp.xml', TEncoding.UTF8);
    //
      phz:='Attempting to load "temp.html" as XML...';
    z.LoadFromFile(getcurrentdir+'\data\temp.xml');
      phz:='Attempting to save loaded XML as "temp.xml"...';
    z.Root.SetAttribute('dateTime', formatdatetime('yyyy/mmm/dd hh:nn:ss', now));
    z.SaveToFile(getcurrentdir+'\data\temp.xml');
      phz:='Attempting to load newly-saved "temp.xml" (practically, re-load the data)...';
    z.LoadFromFile(getcurrentdir+'\data\temp.xml');
    //
    phz:='Attempting conversion (getting <tr> list)...';
    zl:=z.Root.Find('table').FindNodes('tr');
    nur:=0; for p:=1 to zl.Count-1 do if zl[p].ChildNodes.Count>1 then inc(nur);
    setlength(ur, nur);
    phz:='Attempting conversion (converting <tr>''s)...';
    for p:=0 to nur-1 do
      begin               //showmessagefmt('p=%d/%d', [p, nur]);
        phz:='Attempting to extract result '+inttostr(p)+' / '+inttostr(nur)+'...';
        phz2:='ID';         ur[p].ID    := inttostr(GetOnlyNumber(zl[p+1].ChildNodes[0].Text));
        phz2:='Date&Time';  ur[p].When  := GetDateTimeFromHTMLText(zl[p+1].ChildNodes[1].Text, zl[p+1].ChildNodes[2].Text);
        phz2:='HomeT';      ur[p].HomeT := IdentifyTeam(zl[p+1].ChildNodes[3].Text);
        phz2:='AwayT';      ur[p].AwayT := IdentifyTeam(zl[p+1].ChildNodes[5].Text);
        phz2:='HTML addr';  if zl[p+1].ChildNodes[6].Find('a')=nil then
                              ur[p].Attendance.HTMLMatchAddress := GetMatchSheetHTMLAddress('')
                            else
                              if not zl[p+1].ChildNodes[6].Find('a').HasAttribute('href') then
                                ur[p].Attendance.HTMLMatchAddress := GetMatchSheetHTMLAddress('')
                              else
                                ur[p].Attendance.HTMLMatchAddress := GetMatchSheetHTMLAddress(zl[p+1].ChildNodes[6].Find('a').Attribute['href']);
        phz2:='Score';      if zl[p+1].ChildNodes[6].Find('a')<>nil then GetResultFromHTML(zl[p+1].ChildNodes[6].Find('a').Text, ur[p])
                            else GetResultFromHTML('x', ur[p])
      end;
    //
    phz:='Everything from extraction should have finished by now.'; phz2:='!';
  except on E:Exception do begin
    MessageDlg('An error has occured while extracting from file "temp.html" (or "temp.xml"). See if the error message below points towards a solution.'+dnl+E.ClassName+' :  '+E.Message+dnl+'in phase ="'+phz+'" ('+phz2+')', mtError, [mbOK], 0);
    sh2.Brush.Color:=clError; statusL.Caption:=' An error occured while extracting data '; timeoutT.Enabled:=false; Exit end
  end;
  sh2.Brush.Color:=clSuccess; statusL.Caption:=' Extracted data! Finalizing... '; finishT.Enabled:=true
end;

procedure TFUpdate.finishTTimer(Sender: TObject);
var i: word;
begin
  finishT.Enabled:=false;
  try
    z:=TXMLVerySimple.Create; z.Root.NodeName:='MolLeagueV2byBogdyBBA';
    z.Root.SetAttribute('lastUpdated', formatdatetime('ddd, d mmm yyyy, h:nn', now));
    z.Root.SetAttribute('note', 'Don''t modify any of this file unless you know what you''re doing!!!');
    z.Root.ChildNodes.Add(Sett); z.Root.ChildNodes.Add(T); z.Root.AddChild('Results');
    for i:=0 to nur-1 do
      begin
        z.Root.Find('Results').AddChild('result');
        with z.Root.Find('Results').ChildNodes.Last do
          begin
            SetAttribute('ID', ur[i].ID);
            SetAttribute('when', formatdatetime('yyyy/mm/dd hh:nn', ur[i].When));
            SetAttribute('teams', ur[i].HomeT+'-'+ur[i].AwayT);
            SetAttribute('played', booleantotruefalse(ur[i].Played));
            if ur[i].ForceResult.Force then
            	SetAttribute('forceResult', Format('%d-%d', [ur[i].ForceResult.HomeG, ur[i].ForceResult.AwayG]));
            SetAttribute('result', EncodeResult(ur[i]));
            if X.Root.Find('Results', 'ID', ur[i].ID)<>nil then
              if X.Root.Find('Results', 'ID', ur[i].ID).HasAttribute('attendance') then
                SetAttribute('attendance', X.Root.Find('Results', 'ID', ur[i].ID).Attribute['attendance']);
            if not HasAttribute('attendance') then SetAttribute('attendance', '0');
            SetAttribute('sheetHTML', ur[i].Attendance.HTMLMatchAddress)
          end;
      end;
    if not CopyFile(PWideChar(getcurrentdir+'\data\data.xml'), PWideChar(getcurrentdir+'\data\backups\data '+formatdatetime('yyyy.mm.dd hh.nn', now)+'.xml'), false) then
      if MessageDlg('The file "data\data.xml" could not be backed up.'+dnl+'Would you like to continue?', mtWarning, [mbYes, mbCancel], 0)=mrCancel then
        begin sh3.Brush.Color:=clError; statusL.Caption:=' The process was cancelled by YOU. '; timeoutT.Enabled:=false; Exit end;
    z.SaveToFile(getcurrentdir+'\data\data.xml');
  except on E:Exception do begin
    MessageDlg('An error has occured while finishing up. See if the error message below points towards a solution.'+dnl+E.ClassName+' :  '+E.Message, mtError, [mbOK], 0);
    sh3.Brush.Color:=clError; statusL.Caption:=' An error occured while finalizing '; timeoutT.Enabled:=false; Exit end
  end;
  sh3.Brush.Color:=clSuccess; timeoutT.Enabled:=false; statusL.Caption:=' Successfully updated data! Restart to see. '
end;

end.
