object FUpdate: TFUpdate
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FUpdate'
  ClientHeight = 256
  ClientWidth = 614
  Color = 4209709
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 24
  object Panel1: TPanel
    Left = 16
    Top = 16
    Width = 581
    Height = 224
    BevelWidth = 4
    TabOrder = 0
    object Label7: TLabel
      Left = 16
      Top = 10
      Width = 156
      Height = 34
      Caption = 'Update data'
      Font.Charset = ANSI_CHARSET
      Font.Color = 2861823
      Font.Height = -27
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 42
      Width = 458
      Height = 18
      Caption = 
        'The program will try to automatically update the data with the l' +
        'atest results.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu Medium'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 58
      Width = 436
      Height = 18
      Caption = 
        'Existing data will be backed up. Be sure you'#39're connected to the' +
        ' internet.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu Medium'
      Font.Style = []
      ParentFont = False
    end
    object sh1: TShape
      Left = 40
      Top = 90
      Width = 15
      Height = 15
      Brush.Color = 4227327
      Pen.Color = clWhite
    end
    object Label2: TLabel
      Left = 75
      Top = 90
      Width = 434
      Height = 15
      Caption = 'Downloading http://icehockey.hu/oldalak/mol_liga/menetrend ...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu Mono'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object sh2: TShape
      Left = 40
      Top = 119
      Width = 15
      Height = 15
      Brush.Color = clRed
      Pen.Color = clWhite
    end
    object Label3: TLabel
      Left = 75
      Top = 119
      Width = 238
      Height = 15
      Caption = 'Extracting and converting data ...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu Mono'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object sh3: TShape
      Left = 40
      Top = 148
      Width = 15
      Height = 15
      Brush.Color = 65408
      Pen.Color = clWhite
    end
    object Label5: TLabel
      Left = 75
      Top = 148
      Width = 126
      Height = 15
      Caption = 'Saving results ...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu Mono'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object statusL: TLabel
      Left = 16
      Top = 186
      Width = 18
      Height = 18
      Caption = '      '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Ubuntu Medium'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object closeB: TButton
      Left = 408
      Top = 176
      Width = 153
      Height = 34
      Caption = 'Close'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -21
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = closeBClick
    end
  end
  object downloadT: TTimer
    Enabled = False
    OnTimer = downloadTTimer
    Left = 56
    Top = 192
  end
  object timeoutT: TTimer
    Enabled = False
    Interval = 30000
    OnTimer = timeoutTTimer
    Left = 384
    Top = 192
  end
  object extractT: TTimer
    Enabled = False
    OnTimer = extractTTimer
    Left = 120
    Top = 192
  end
  object finishT: TTimer
    Enabled = False
    OnTimer = finishTTimer
    Left = 184
    Top = 192
  end
end
