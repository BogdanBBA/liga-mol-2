unit DataTypes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, jpeg, PngImage, ImgList,
  WinINet, ComCtrls, MPlayer, ToolWin, FastHtmlParser, XML.VerySimple, UIntList;

const
  nl=#13#10; dnl=nl+nl;
  KB=1024; MB=KB*KB;
  NFo: array[0..1] of string=('data', 'data\backups');
  NFi: array[0..1] of string=('data\data.xml', 'data\settings.xml');

type TResult=record
  ID, HomeT, AwayT: string;
  When: TDateTime;
  Played, homeWin: boolean;
  ResultHG, ResultAG, nPeriods: word;
  ForceResult: record
		Force: boolean;
    HomeG, AwayG: word;
  end;
  Periods: array[1..5] of record
    HomeG, AwayG: word;
  end;
  Attendance: record
    HTMLMatchAddress: string;
    Value: word;
  end;
end;

type TStandingsPosition=record
  TeamID: string;
  M, p3, p2, p1, p0, GF, GA, GDiff, Pts: integer;
end;


type TValStats=record
	count, min, max: word;
  total: longint;
  avg: real;
end;

var
  T, Sett: TXMLNode;
  rs: array of TResult; //rs=regular season;
  st: array of TStandingsPosition;

  nt, nr: word; //nr=number of regular season results;

  favT: string;
  f: textfile;
  x, xSett: TXMLverySimple;

procedure log(s: string);
procedure ResetValStats(var x: TValStats);
function Plural(SingularForm: string; Quantity: longint; includeQuantity: boolean): string;
function grupat(x: int64): string;
function GetInetFile (const fileURL, FileName: String): boolean;
function ResultPosByID(ID: string): integer;
function StandingsPosForTeam(ID: string): integer;
procedure DecodePair(s: string; sep: char; var a, b: string); Overload;
procedure DecodePair(s: string; sep: char; var a, b: word); Overload;
procedure DecodeResult(s: string; var r: TResult);
function GetTeamColor(TeamID: string; firstCol: boolean): TColor;
function EncodeResult(r: Tresult): string;
function FormatScore(ri: word): string;
function FormatPeriodScores(ri: word): string;
function ScoreSubtitle(ri: word): string;
function NResultPoints(r: TResult; forTeam: string): word;
procedure InitializeAndReadData;
procedure CheckData;

implementation

uses Mol2pas, Team, functii;

procedure log(s: string);
var logf: textfile;
begin
  try
    F1.logL.Caption:=s+formatdatetime(' (d mmm yyyy, hh:nn)', now);
    assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    ', now), s); closefile(logf)
  except
    begin
      try
        F1.logL.Caption:=s+formatdatetime('"(Log retry, "d mmm yyyy, hh:nn)', now);
        assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    "Log retry: "', now), s); closefile(logf)
      except on E:Exception do MessageDlg('Failed a second time to log message "'+s+'"'+dnl+e.classname+' :  '+e.Message, mtError, [mbOK], 0) end
    end
  end
end;

procedure ResetValStats(var x: TValStats);
begin x.count:=0; x.min:=high(word); x.max:=0; x.total:=0; x.avg:=0 end;

function Plural(SingularForm: string; Quantity: longint; includeQuantity: boolean): string;
var r: string;
begin
  r:=SingularForm;
  case strtocase(r, ['match', 'has', 'error']) of
  0: if Quantity<>1 then r:=r+'es';
  1: if Quantity<>1 then r:='have';
  2: if Quantity<>1 then r:='s';
  else MessageDlg('Plural(SingularForm="'+SingularForm+'") ERROR: Unknown SingularForm -> the plural form may be incorrect.', mtWarning, [mbOK], 0)
  end;
  if includeQuantity then r:=inttostr(Quantity)+' '+r;
  result:=r
end;

function grupat(x: int64): string;
  function invers(s: string): string;
  var i: word; t: string;
  begin for i:=1 to length(s) do t:=s[i]+t; invers:=t end;
var s, t: string;
begin
  s:=invers(inttostr(x)); t:='';
  while s<>'' do
    begin
      t := invers(copy(s, 1, 3)) + FormatSettings.ThousandSeparator + t;
      delete(s, 1, 3)
    end;
  delete(t, length(t), 1); grupat:=t
end;

function GetInetFile (const fileURL, FileName: String): boolean;
const BufferSize = 1024;
var hSession, hURL: HInternet; Buffer: array[1..BufferSize] of Byte; BufferLen: DWORD; f: File; sAppName: string;
begin
  try
  result := false;
  sAppName := ExtractFileName(Application.ExeName) ;
  hSession := InternetOpen(PChar(sAppName), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0) ;
  try
    hURL := InternetOpenURL(hSession, PChar(fileURL), nil, 0, 0, 0) ;
    try
      AssignFile(f, FileName) ;
      Rewrite(f,1) ;
      repeat
        InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) ;
        BlockWrite(f, Buffer, BufferLen)
      until BufferLen = 0;
      CloseFile(f) ;
      result := True;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end
  except on E:Exception do begin
    result:=false;
    MessageDlg('An error has occured while downloading from "'+fileURL+'" to file "'+FileName+'". See if the error message below points towards a solution.'+dnl+E.ClassName+' :  '+E.Message, mtError, [mbOK], 0)
  end end
end;

function ResultPosByID(ID: string): integer;
var i: word;
begin result:=-1; for i:=0 to nr-1 do if rs[i].ID=ID then begin result:=i; break end end;

function StandingsPosForTeam(ID: string): integer;
var i: word;
begin result:=-1; for i:=0 to nt-1 do if st[i].TeamID=ID then begin result:=i; break end end;

procedure DecodePair(s: string; sep: char; var a, b: string); Overload;
begin try a:=copy(s, 1, pos(sep, s)-1); b:=copy(s, pos(sep, s)+1, length(s)-pos(sep, s)) except on E:Exception do showmessage('An error occured in DeocdePair (string) (s="'+s+'", sep="'+sep+'")'+dnl+E.ClassName+' - '+E.Message) end end;

procedure DecodePair(s: string; sep: char; var a, b: word); Overload;
begin try a:=strtoint(copy(s, 1, pos(sep, s)-1)); b:=strtoint(copy(s, pos(sep, s)+1, length(s)-pos(sep, s))) except on E:Exception do showmessage('An error occured in DeocdePair (word) (s="'+s+'", sep="'+sep+'")'+dnl+E.ClassName+' - '+E.Message) end end;

procedure DecodeResult(s: string; var r: TResult);
var hg, ag: word;
begin
  try
  StrCleanup(s, true, true); if s[length(s)]<>';' then s:=s+';'; r.nPeriods:=0; hg:=0; ag:=0;
  while s<>'' do
    begin
      inc(r.nPeriods);
      with r.Periods[r.nPeriods] do
        begin DecodePair(copy(s, 1, pos(';', s)-1), '-', HomeG, AwayG); inc(hg, HomeG); inc(ag, AwayG) end;
      delete(s, 1, pos(';', s))
    end;
  r.ResultHG:=hg; r.ResultAG:=ag; r.homeWin:=hg>ag
  except on E:Exception do showmessage('An error occured in DecodeResult(s="'+s+'")'+dnl+E.ClassName+' - '+E.Message) end
end;

function GetTeamColor(TeamID: string; firstCol: boolean): TColor;
var a, b: string;
begin
  DecodePair(T.Find('team', 'ID', TeamID).Attribute['colors'], ',', a, b);
  if firstCol then result:=HTMLToColor(a) else result:=HTMLToColor(b)
end;

function FormatScore(ri: word): string;
begin
  if rs[ri].Played then
  	begin
    	if rs[ri].ForceResult.Force then
				result:=Format('%d-%d', [rs[ri].ForceResult.HomeG, rs[ri].ForceResult.AwayG])
      else
      	result:=Format('%d-%d', [rs[ri].ResultHG, rs[ri].ResultAG])
    end
  else result:='-'
end;

function FormatPeriodScores(ri: word): string;
var r: string; i: word;
begin
  if not rs[ri].Played then begin result:='---'; exit end;
  if rs[ri].ForceResult.Force then
  	result:='jury'
  else
  	begin
		  r:=''; for i:=1 to rs[ri].nPeriods do r:=Format('%s%d-%d; ', [r, rs[ri].Periods[i].HomeG, rs[ri].Periods[i].AwayG]);
      result:=copy(r, 1, length(r)-2)
    end
end;

function EncodeResult(r: Tresult): string;
var i: word; rz: string;
begin rz:=''; for i:=1 to r.nPeriods do rz:=rz+inttostr(r.Periods[i].HomeG)+'-'+inttostr(r.Periods[i].AwayG)+';'; result:=copy(rz, 1, length(rz)-1) end;

function ScoreSubtitle(ri: word): string;
var a, b, c: string;
begin
	if rs[ri].ForceResult.Force then begin result:='The match finished with a decision of the jury'; exit end;
  case rs[ri].nPeriods of 3: a:='in regular time'; 4: a:='in extra-time'; 5: a:='at penalties'; else a:='ERROR(nPeriods='+inttostr(rs[ri].nPeriods)+')' end;
  if rs[ri].homeWin then begin b:='home'; c:=rs[ri].HomeT end else begin b:='guest'; c:=rs[ri].AwayT end;
  result:='The match finished '+a+' and was won by the '+b+' team, '+c
end;

function NResultPoints(r: TResult; forTeam: string): word;
var a, b: word;
begin
  if r.ResultHG>r.ResultAG then begin if r.nPeriods=3 then begin a:=3; b:=0 end else begin a:=2; b:=1 end end
  else begin if r.nPeriods=3 then begin a:=0; b:=3 end else begin a:=1; b:=2 end end;
  if r.ForceResult.Force then begin if r.ForceResult.HomeG>r.ForceResult.AwayG then begin a:=3; b:=0 end else begin a:=0; b:=3 end end;
  if forTeam=r.HomeT then result:=a else if forTeam=r.AwayT then result:=b else MessageDlg('NResultPoints(forTeam="'+forTeam+'") ERROR: team did not play in this match ('+r.HomeT+'-'+r.AwayT+'), returning 0', mtError, [mbIgnore], 0)
end;

procedure InitializeAndReadData;
var i, j: word; xs: string; xr: TResult; xl: TXMLNodeList;
begin
  try
  for i:=0 to high(NFo) do if not directoryexists(NFo[i]) then begin showmessage('The folder "'+NFo[i]+'" does not exist. That is a big error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  for i:=0 to high(NFi) do if not fileexists(NFi[i]) then begin showmessage('The file "'+NFi[i]+'" does not exist. That is a big error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  with FormatSettings do begin DecimalSeparator:='.'; ThousandSeparator:=','; DateSeparator:='/'; TimeSeparator:=':' end;
  assignfile(f, 'data\log.txt'); rewrite(f); closefile(f); FontCheck(false, ['Ubuntu', 'Ubuntu Mono', 'Ubuntu Condensed', 'Segoe UI', 'Segoe UI Light', 'Signika']);
  log('Initializing data...');
  //
  log('Reading data...');
  x:=TXMLverySimple.Create; xSett:=TXMLverySimple.Create;
  xSett.LoadFromFile('data\settings.xml'); Sett:=xSett.Root.Find('Settings'); favT:=Sett.Find('FavoriteTeam').Attribute['ID'];
  x.LoadFromFile('data\data.xml');
  T:=x.Root.Find('Teams'); nt:=T.ChildNodes.Count; setlength(st, nt);
  xl:=x.Root.Find('Results').FindNodes('result'); nr:=xl.Count; setlength(rs, nr);
  //
  for i:=0 to nr-1 do
    try
      rs[i].ID:=xl[i].Attribute['ID']; rs[i].When:=GetDate(xl[i].Attribute['when']);
      rs[i].Played:=truefalsetoboolean(xl[i].Attribute['played'], true);
      DecodePair(xl[i].Attribute['teams'], '-', rs[i].HomeT, rs[i].AwayT);
      rs[i].ForceResult.Force:=xl[i].HasAttribute('forceResult');
      if rs[i].ForceResult.Force then
      	begin xs:=xl[i].Attribute['forceResult']; rs[i].ForceResult.HomeG:=StrToInt(Copy(xs, 1, pos('-',xs)-1)); rs[i].ForceResult.AwayG:=StrToInt(Copy(xs, pos('-',xs)+1, length(xs))) end;
      DecodeResult(xl[i].Attribute['result'], rs[i]);
      rs[i].Attendance.HTMLMatchAddress:=xl[i].Attribute['sheetHTML'];
      rs[i].Attendance.Value:=strtoint(xl[i].Attribute['attendance']);
    except on E:Exception do MessageDlg('An error has occured while reading RESULT i = '+inttostr(i)+dnl+E.ClassName+': '+E.message+dnl+'The program will try to continue.', mtError, [mbOK], 0) end;
  if nr>1 then
    for i:=0 to nr-2 do for j:=i+1 to nr-1 do
      if rs[i].When>rs[j].When then begin xr:=rs[i]; rs[i]:=rs[j]; rs[j]:=xr end;
  log('Data read successfully (nt='+inttostr(nt)+', nr='+inttostr(nr)+') !')
  except on E:Exception do begin MessageDlg('ERROR in InitializeAndReadData'+dnl+E.ClassName+': '+E.message, mtError, [mbOK], 0); exit end end;
  CheckData
end;

procedure CheckData;
var i: word; xs: string; idl: TIntList;
begin
  try
  if not fileexists('data\img\harta2.png') then MessageDlg('Map does not exist (file "data\img\harta2.png") !', mtError, [mbIgnore], 0);
  if nt=0 then MessageDlg('Number of teams is 0 !', mtError, [mbIgnore], 0);
  if nr=0 then MessageDlg('Number of results is 0.', mtWarning, [mbOK], 0);
  for i:=0 to nt-1 do
    begin
      try xs:=t.ChildNodes[i].Attribute['ID'] except MessageDlg('Invalid "ID" attribute for team i='+inttostr(i)+' !', mtError, [mbIgnore], 0) end;
      try xs:=t.ChildNodes[i].Attribute['city'] except MessageDlg('Invalid "city" attribute for team i='+inttostr(i)+' !', mtError, [mbIgnore], 0) end;
      try xs:=t.ChildNodes[i].Attribute['fullName'] except MessageDlg('Invalid "fullName" attribute for team i='+inttostr(i)+' !', mtError, [mbIgnore], 0) end;
      try xs:=t.ChildNodes[i].Attribute['mapXY']; strtoint(copy(xs, 1, pos(',', xs)-1)); strtoint(copy(xs, pos(',', xs)+1, length(xs)-pos(',', xs))) except MessageDlg('Invalid "mapXY" attribute for team i='+inttostr(i)+' !', mtError, [mbIgnore], 0) end;
      if not fileexists('data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png') then MessageDlg('Image file "data\img\teams\'+t.ChildNodes[i].Attribute['ID']+'.png" does not exist !', mtError, [mbIgnore], 0);
    end;
  if T.Find('team', 'ID', favT)=nil then MessageDlg('Invalid favorite team ID = "'+favT+'" !', mtError, [mbIgnore], 0);
  idl:=TIntList.Create; idl.Sorted:=false;
  for i:=0 to nr-1 do
    begin
      try if idl.IndexOf(strtoint(rs[i].ID))<>-1 then MessageDlg('Result "ID" attribute already exists (ID='+rs[i].ID+', at result i='+inttostr(i)+') !', mtError, [mbIgnore], 0) else idl.Add(strtoint(rs[i].ID)) except MessageDlg('Invalid "ID" attribute for result i='+inttostr(i)+' (ID="'+rs[i].ID+'") !', mtError, [mbIgnore], 0) end;
      if t.Find('team', 'ID', rs[i].HomeT)=nil then MessageDlg('Invalid home team ("'+rs[i].HomeT+'") for result i='+inttostr(i)+' !', mtError, [mbIgnore], 0);
      if t.Find('team', 'ID', rs[i].AwayT)=nil then MessageDlg('Invalid away team ("'+rs[i].AwayT+'") for result i='+inttostr(i)+' !', mtError, [mbIgnore], 0);
      if not (rs[i].nPeriods in [3..5]) then MessageDlg('Invalid number of periods ('+inttostr(rs[i].nPeriods)+') for result i='+inttostr(i)+' !', mtError, [mbIgnore], 0);
      if rs[i].Played and (not rs[i].ForceResult.Force) and (rs[i].ResultHG=rs[i].ResultAG) then MessageDlg('The match can not end in a tie ('+inttostr(rs[i].ResultHG)+'-'+inttostr(rs[i].ResultAG)+') - result i='+inttostr(i)+', ID="'+rs[i].ID+'" !', mtError, [mbIgnore], 0);
    end
  except on E:Exception do begin MessageDlg('ERROR in CheckData'+dnl+E.ClassName+': '+E.message, mtError, [mbOK], 0); exit end end
end;

end.
